import pandas as pd
import py2neo as pn
import numpy as np
import time
import matplotlib.pyplot as plt
import datetime
import configparser
import os
from scipy.linalg import eig

#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)
# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
#data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB = config['database']['EntityIdAttributeNameDB']

# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset

def conn():
    """
    Connect to Neo4j

    Parameters: None

    Returns: py2neo.database.Graph: A Py2neo Graph object representing the connection to the Neo4j graph database
    """
    try:
        # connect DB
        return pn.Graph(neo4j_url,auth=(username_neo4j, password_neo4j))
    except Exception as error:
        print('Caught this error: ' + repr(error))

def GetTimeline(graph,EntityLabel):
    """
    Retrieve the start and end times for a timeline of entities with a given label.

    Parameters:
    graph (py2neo.database.Graph): A Py2neo Graph object representing the connection to the Neo4j graph database.
    EntityLabel (str): A string that specifies the label of the entities for which the timeline is to be retrieved.

    Returns:
    pandas.DataFrame: A Pandas DataFrame with two columns, "min" and "max", which contain the minimum start time and maximum end time, respectively, of the entities with the specified label in the graph database.
    """
    # query for timeline of entities with given label
    query_max_min = f"MATCH (n:{EntityLabel})-[r]-()" +\
                " RETURN min(r.startvalidtime) as min, max(r.endvalidtime) as max"
    print(query_max_min)
    print(graph.run(query_max_min).to_data_frame())
    return graph.run(query_max_min).to_data_frame()

def SplitTimeline(TimeUnit, result_min_max=None, IntervalStartUnit=None, IntervalEndUnit=None):
    """
    Splits a timeline into intervals of a given time unit.

    Parameters:
    TimeUnit (str): A string that specifies the time unit used for splitting the timeline, e.g. "D" for days, "M" for months, etc.
    result_min_max (pandas.DataFrame, optional): A Pandas DataFrame with two columns, "min" and "max", which contain the minimum start time and maximum end time, respectively, of the timeline. If not provided, this function will assume that the entire timeline should be split.
    IntervalStartUnit (str, optional): A string that specifies the start time of the interval to be split. If provided, this function will split the interval between this time and the time specified in IntervalEndUnit.
    IntervalEndUnit (str, optional): A string that specifies the end time of the interval to be split. If provided, this function will split the interval between this time and the time specified in IntervalStartUnit.

    Returns:
    pandas.DatetimeIndex: A DatetimeIndex object containing the start times of each interval in the timeline, split according to the specified time unit.
    """
    if (IntervalStartUnit is None) & (IntervalEndUnit is None):
        tmin = pd.to_datetime(result_min_max["min"][0]).strftime('%Y-%m-%dT00:00:00')
        tmax = (pd.to_datetime(result_min_max["max"][0])+ pd.Timedelta(days=1)).strftime('%Y-%m-%dT00:00:00')
        # split the timeline with given time unit
        return pd.date_range(tmin, tmax, freq=TimeUnit)
    else:
        start_interval_datetime = pd.to_datetime(IntervalStartUnit)
        end_interval_datetime = pd.to_datetime(IntervalEndUnit)
        end_interval_end_datetime = end_interval_datetime + pd.Timedelta(days=1)
        return pd.date_range(start_interval_datetime, end_interval_end_datetime, freq=TimeUnit)

def MatchEntityId(graph,EntityLabel):
    # query to match entity id and state id
    query_entity_node = f"MATCH (n: {EntityLabel})" + f" RETURN n.{EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId"
    return graph.run(query_entity_node).to_data_frame()

def EigenvectorInUnit(graph, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):
    """
    Calculate the sum of relationships with given relationship type (if specified)existent within the given time unit as the degree of entities with the given label.

    Parameters:
    - graph: The Neo4j graph object on which the query is run.
    - UnitStartTime: The start time of the unit for which the degree is to be calculated.
    - UnitEndTime: The end time of the unit for which the degree is to be calculated.
    - EntityLabel: The label of the entity for which the degree is to be calculated.
    - RelationshipTypeStr (optional): The type of relationship between the entities. If not provided, all relationships are considered.

    Returns:
    - A Pandas DataFrame object containing the EntityId and DegreeEntity columns, where EntityId is the ID of the entity and DegreeEntity is the degree of the entity in the given time unit.
    """
    # query for degree in one unit, maybe with relationships chosen
    query_adjacency = f"""MATCH (n){'-' + f'[r:{RelationshipTypeStr}]' if RelationshipTypeStr else '-[r]'}-(m) WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN id(n) AS NodeId, COLLECT(DISTINCT(id(m))) AS AdjacentNodes"""
    print(query_adjacency)
    df = graph.run(query_adjacency).to_data_frame()
    adj_matrix = np.zeros((len(df), len(df)))
    print('MC')
    # Function to map linked node IDs to their indices using the DataFrame's inherent index
    def map_indices(row, df):
        return [df.index[df['NodeId'] == node].tolist()[0] for node in row['AdjacentNodes']]
    # Apply the function to create a new column
    df['entity_mapping'] = df.apply(map_indices, axis=1, args=(df,))
    for idx, row in df.iterrows():
        for node in row['entity_mapping']:
            adj_matrix[idx - 1][node - 1] += 1
    # Calculate eigenvectors and eigenvalues
    eigenvalues, eigenvectors = eig(adj_matrix)
    # Find the index of the eigenvalue with the largest magnitude
    max_index = np.argmax(np.abs(eigenvalues))
    # Get the corresponding eigenvector
    max_eigenvector = eigenvectors[:, max_index]
    # Normalize the eigenvector
    eigenvector_centrality = max_eigenvector / np.sum(max_eigenvector)
    # Add eigenvector centrality values as a new column to the dataframe
    df['Eigenvector'] = eigenvector_centrality.real
    return df.loc[:,['NodeId','Eigenvector']].copy() 

def EigenvectorOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit = None, IntervalEndUnit = None, RelationshipType=None):
    """
    Get the degree of each entity with the given label over all time units on the timeline.

    Parameters:
        graph (py2neo.database.Graph): Graph database object to query.
        EntityLabel (str): Label of the entity for which degree is calculated.
        TimeUnit (str): Time unit for which the degree is calculated (e.g. "D" for day, "M" for month).
        IntervalStartUnit (str, optional): Start of the time interval for which the degree is calculated.
        IntervalEndUnit (str, optional): End of the time interval for which the degree is calculated.
        RelationshipType (list, optional): Types of relationships to include in the degree calculation. If None, all relationships are included.

    Returns:
        pd.DataFrame: A DataFrame with the degree of each entity with the given label over time.
    """
     # create the list of time units on the timeline
    if (IntervalStartUnit is None) & (IntervalEndUnit is None):
        time_axis = SplitTimeline(TimeUnit,result_min_max=GetTimeline(graph, EntityLabel))
    else :
        time_axis = SplitTimeline(TimeUnit,IntervalStartUnit = IntervalStartUnit, IntervalEndUnit = IntervalEndUnit)
    NodeEigenvector = []
    # if there is no restriction on relationship type
    if RelationshipType is None:
        # calculate degree centrality for every entity in every unit
        for i in range(0, len(time_axis)):
            t = datetime.datetime.isoformat(time_axis[i])
            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                df = EigenvectorInUnit(graph, t, t_after)
                df.set_index('NodeId', inplace=True)
                df.columns = [str(t)]
            except:
                continue
            # collect result
            NodeEigenvector.append(df)
    # if there is a selection on relationship type
    else:
        # unify the order of relationship names
        RelationshipType.sort()
        # transform them to string with the separator for Neo4j CQL
        RelationshipTypeStr = '|'.join(RelationshipType)
        for i in range(0, len(time_axis)):
            # calculate degree centrality for every entity in every unit
            t = datetime.datetime.isoformat(time_axis[i])
            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                df = EigenvectorInUnit(graph, t, t_after)
                df.set_index('NodeId', inplace=True)
                df.columns = [str(t)]
            except:
                continue
            NodeEigenvector.append(df)
    NodeEigenvector_df = pd.concat(NodeEigenvector, axis=1)
    NodeEigenvector_df.fillna(0, inplace=True)
    return NodeEigenvector_df

def Position(EntitiesEigenvector, EntityIdentifier=None, show_distribution=True):
    """
    Sort the temporal degree centrality of all entities with the same label and show their distribution if required.

    Parameters:
        EntitiesDegree (pd.DataFrame): A DataFrame with the temporal degree of each entity with the given label.
        EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
        show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
        None
    """
    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show degree centrality of every entity with the label chosen and their distribution
        EntitiesEigenvector.columns=['Eigenvector']
        EntitiesEigenvector=EntitiesEigenvector.sort_values(by='Eigenvector',ascending=False)
        EntitiesEigenvector['Rank']=range(1,EntitiesEigenvector.shape[0]+1)
        if show_distribution :
            EntitiesEigenvector['Eigenvector'].plot(kind='box')
            plt.savefig('EigenvectorDistribution.pdf', bbox_inches='tight', format='pdf')
            EntitiesEigenvector['Position']=EntitiesEigenvector['Rank']/EntitiesEigenvector.shape[0]
        print(EntitiesEigenvector)
    # if it is not for a special time interval
    else:
        # show degree centrality of chosen entity with its rank and position among all entities with the same label
        EntitiesEigenvector.columns=['Eigenvector']
        EntitiesEigenvector=EntitiesEigenvector.sort_values(by='Eigenvector',ascending=False)
        EntitiesEigenvector['Rank']=range(1,EntitiesEigenvector.shape[0]+1)
        if show_distribution :
            EntitiesEigenvector['Eigenvector'].plot(kind='box')
            plt.axhline(y=EntitiesEigenvector.loc[EntityIdentifier].iloc[0],ls='--',label='Entity')
            plt.legend()
            plt.savefig('EigenvectorDistribution.pdf', bbox_inches='tight', format='pdf')
            plt.show(block=False)
            EntitiesEigenvector['Position']=EntitiesEigenvector['Rank']/EntitiesEigenvector.shape[0]
        print(EntitiesEigenvector.loc[EntityIdentifier])

def Evolution(EigenvectorInEveryUnit, EntityIdentifier):
    """
    Plots the degree evolution of an entity over time.

    Parameters:
        DegreeInEveryUnit (pd.DataFrame): A DataFrame with the temporal degree of each entity.
        EntityIdentifier (str): The identifier of the entity for which to plot the degree evolution.

    Returns:
        None
    """
    # show degree evolution of an entity in its valid time
    ax = EigenvectorInEveryUnit.loc[EntityIdentifier].plot()
    # Set the rotation angle of x-axis labels
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    plt.savefig('EigenvectorEvolution.pdf', bbox_inches='tight', format='pdf')
    # Display the plot
    plt.show(ax)

def EigenvectorCentrality(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Calculates the degree centrality of entities over time and returns the results in various formats.

    Parameters:
    EntityLabel (str): The label of the entity to analyze.
    TimeUnit (str): The time unit to use for analysis (e.g. 'm' for month, 'w' for week, 'd' for day).
    RelationshipType (list of str, optional): The types of relationships to consider in analysis. If not specified, all relationship types will be considered.
    EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
    IntervalStartUnit (str, optional): The start time unit of a time interval to analyze (inclusive).
    IntervalEndUnit (str, optional): The end time unit of a time interval to analyze (inclusive).
    show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
    None
    """

    # connect to database
    graph=conn()

    df_entity_id=MatchEntityId(graph,EntityLabel)
    df_eigenvector = EigenvectorOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit, IntervalEndUnit, RelationshipType)
    df_eigenvector = EigenvectorOnTimeline(graph, EntityLabel, RelationshipType)
    df_eigenvector_reset = df_eigenvector.reset_index()
    df = pd.merge(df_entity_id, df_eigenvector_reset, on='NodeId', how='inner')
    df.drop('NodeId', axis=1, inplace=True)
    df = df.groupby(['EntityId']).sum()
    # clear the query cache
    graph.run('CALL db.clearQueryCaches()')

    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show the distribution curve and degree table
        Position(pd.DataFrame(df.mean(axis=1)),show_distribution=show_distribution)
    # if it is not for a special time interval
    else :
        # show degree evolution of an entity in its valid time
        Evolution(df, EntityIdentifier)
        # show the distribution curve and the position of the entity
        Position(pd.DataFrame(df.mean(axis=1)),EntityIdentifier,show_distribution=show_distribution)

def timing(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Call the function DegreeCentrality to calculate the running time, parameters same as DegreeCentrality.

    Parameters:
    EntityLabel (str): The label of the entity to analyze.
    TimeUnit (str): The time unit to use for analysis (e.g. 'm' for month, 'w' for week, 'd' for day).
    RelationshipType (list of str, optional): The types of relationships to consider in analysis. If not specified, all relationship types will be considered.
    EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
    IntervalStartUnit (str, optional): The start time unit of a time interval to analyze (inclusive).
    IntervalEndUnit (str, optional): The end time unit of a time interval to analyze (inclusive).
    show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
    None
    """
    time_start = time.time()
    EigenvectorCentrality(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution)
    time_end = time.time()
    time_c = time_end - time_start
    print('time cost', time_c, 's')
    return(time_c)

EigenvectorCentrality("Student","d",RelationshipType=["Call"],show_distribution=False)
