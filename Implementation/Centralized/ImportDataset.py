import os
import configparser

#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)

password_admin = config['admin']['password']
dataset_source_path = config['paths']['dataset_source_path']
dataset_target_path = config['paths']['dataset_target_path']


# Delete old csv files
cmd = f'sudo rm -f {dataset_target_path}*.csv'
os.system(f'echo {password_admin}|sudo -S {cmd}')
# copy new csv files in neo4j import folder
cmd = f'sudo cp {dataset_source_path}*.csv {dataset_target_path}'
os.system(f'echo {password_admin}|sudo -S {cmd}')

# Set up the connection to the Neo4j database


def import_social_experiment(database_name):
    """
    Imports data into the Neo4j database.

    Parameters:
        database_name (str): The name of the database to import data into.

    Returns:
        None
    """
    # import data
    cmd = (
        f'sudo neo4j-admin database import full --overwrite-destination '
        f'--nodes={dataset_target_path}Student.csv '
        f'--nodes={dataset_target_path}WLAN.csv '
        f'--relationships={dataset_target_path}Access.csv '
        f'--relationships={dataset_target_path}BlogLivejournalTwitter.csv '
        f'--relationships={dataset_target_path}Call.csv '
        f'--relationships={dataset_target_path}CloseFriend.csv '
        f'--relationships={dataset_target_path}FacebookAllTaggedPhotos.csv '
        f'--relationships={dataset_target_path}PoliticalDiscussant.csv '
        f'--relationships={dataset_target_path}Proximity.csv '
        f'--relationships={dataset_target_path}ReceiveSMS.csv '
        f'--relationships={dataset_target_path}SendSMS.csv '
        f'--relationships={dataset_target_path}SocializeTwicePerWeek.csv '
        f'--skip-bad-relationships --skip-duplicate-nodes --bad-tolerance=1000000 {database_name}'
    )
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))


def import_citibike(database_name):
    # import data
    cmd = (
         f'sudo neo4j-admin database import full --overwrite-destination   ' 
          f' --nodes={dataset_target_path}station.csv ' 
          f'--relationships={dataset_target_path}202102_trip.csv ' 
          f'--relationships={dataset_target_path}202103_trip.csv '
          f'--skip-bad-relationships --skip-duplicate-nodes --bad-tolerance=1000000 {database_name}'
    )
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))
    print(cmd)

def import_e_commerce(database_name):
    """
    Imports data into the Neo4j database.

    Parameters:
        database_name (str): The name of the database to import data into.

    Returns:
        None
    """
    # import data
    cmd = (
          f'sudo neo4j-admin database import full --overwrite-destination  '
          f'--nodes={dataset_target_path}user.csv ' 
          f'--nodes={dataset_target_path}category.csv ' 
          f'--nodes={dataset_target_path}item1_5.csv ' 
          f'--nodes={dataset_target_path}item6_10.csv ' 
          f'--nodes={dataset_target_path}item11_15.csv ' 
          f'--nodes={dataset_target_path}item16_20.csv ' 
          f'--nodes={dataset_target_path}item26_30.csv ' 
          f'--nodes={dataset_target_path}item31_34.csv ' 
          f'--relationships={dataset_target_path}addtocart.csv ' 
          f'--relationships={dataset_target_path}transaction.csv ' 
         f'--relationships={dataset_target_path}view.csv ' 
          f'--relationships={dataset_target_path}belongto.csv ' 
          f'--relationships={dataset_target_path}subCategory.csv ' 
          f'--skip-bad-relationships --skip-duplicate-nodes --bad-tolerance=1000000 {database_name}'
    )
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))




#import_social_experiment("socialexperiment")
import_citibike("citibike")
#import_e_commerce("ecommerce")
