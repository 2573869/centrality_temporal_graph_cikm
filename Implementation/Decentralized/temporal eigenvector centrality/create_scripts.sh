#!/bin/bash

#SBATCH --job-name=create_script
#SBATCH --output=create_script.out
#SBATCH --error=create_script.er

#SBATCH --partition=48CPUNodes

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8

python=/users/sig/song/env/bin/python

script=create_scripts.py 

srun echo "11" | ${python} ${script}
