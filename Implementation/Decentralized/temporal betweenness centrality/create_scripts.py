import pandas as pd
import math
import os
import sys
import numpy as np

def clear_folder(folder_path):
    # Iterate over all files and subdirectories in the folder
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
            # Remove each file
            file_path = os.path.join(root, name)
            os.remove(file_path)
        for name in dirs:
            # Remove each subdirectory
            dir_path = os.path.join(root, name)
            os.rmdir(dir_path)


script_dir = "/users/sig/song/Exp_centrality/Betweenness/balanced/script"
csv_dir = "/users/sig/song/Exp_centrality/Betweenness/balanced/csv"
EntityIdAttributeNameDB = "n.stationid"


def generate_py(nb_machine, IntervalStartUnit, IntervalEndUnit, TimeUnit):
    start_interval_datetime = pd.to_datetime(IntervalStartUnit)
    end_interval_datetime = pd.to_datetime(IntervalEndUnit)
    timeline = pd.date_range(start_interval_datetime, end_interval_datetime, freq=TimeUnit)
    timeline_list = timeline.tolist()
    np.random.shuffle(timeline_list)
    np.random.shuffle(timeline_list)
    np.random.shuffle(timeline_list)
    for i in range(nb_machine):
        sub_tl = timeline_list[i * math.ceil(len(timeline) / nb_machine):min((i + 1) * math.ceil(len(timeline) / nb_machine), len(timeline))+1]
        sub_tl_str = sub_tl.__str__()
        generated_code = ("import neo4j\n"
                          "import pandas as pd\n"
                          "import time\n"
                          "import matplotlib.pyplot as plt\n"
                          "import datetime\n"
                          "import os\n"
                          "from pandas import Timestamp\n"
                          "from neo4j import GraphDatabase, RoutingControl\n"
                          "from neo4j.exceptions import DriverError, Neo4jError\n"
                          "\n"
                          "neo4j_url = \"bolt://localhost:7687\"\n"
                          "AUTH = ('neo4j', '0000')\n"
                          "# in advance, you have to manually change the active database with respect to the volume you want to query\n"
                          "# in neo4j.conf set dbms.memory.max_heap_size = 4G\n"
                          "# Specify the Neo4j installation directory\n"
                          f"neo4j_dir = \"/users/sig/song/neo4j_{str(i + 1)}/bin\"\n"
                          f"csv_dir = \"{csv_dir}\"\n"
                          "# dictionary for dataframes to reduce the time of calculation\n"
                          f"EntityIdAttributeNameDB='{EntityIdAttributeNameDB}'\n"
                          "# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset\n"
                          "\n"
                          "def conn():\n"
                          "    return GraphDatabase.driver(neo4j_url, auth=AUTH)\n"
                          "\n"
                          f"time_axis=pd.DatetimeIndex({sub_tl_str})\n"
                          "print(time_axis)\n"
                          "\n"
                          "def MatchEntityID(driver,EntityLabel):\n"
                          "    # query to match entity id and state id\n"
                          "    query_entity_node = f\"MATCH (n: {EntityLabel})\" + f\" RETURN {EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId\"\n"
                          "    return driver.execute_query(query_entity_node,result_transformer_ = neo4j.Result.to_df)\n"
                          "\n"
                          "def BetweennessInUnit(driver, EntityLabel, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):\n"
                          "\n"
                          "    query_shortest_paths=f'''MATCH (n:{EntityLabel}) WITH collect(n) as Entities CALL apoc.cypher.parallel2(\"MATCH path=AllShortestPaths((n:{EntityLabel}){\"-\" + f\"[r:{RelationshipTypeStr}*]\" if RelationshipTypeStr else \"-[r*]\"}-(m:{EntityLabel})) WITH *, relationships(path) AS rels, nodes(path) as nodes WHERE id(n)<id(m) AND ALL (rel IN rels WHERE datetime(rel.endvalidtime) >= datetime('{UnitStartTime}')) AND ALL (rel IN rels WHERE datetime(rel.startvalidtime) < datetime('{UnitEndTime}')) UNWIND nodes as node RETURN id(n) as start, id(m) as end, Collect(id(node)) as paths \", {{n: Entities}}, 'n') YIELD value RETURN value.start as start, value.end as end, value.paths as paths'''\n"
                          "    print(query_shortest_paths)\n"
                          "    df_paths=driver.execute_query(query_shortest_paths,result_transformer_ = neo4j.Result.to_df)\n"
                          "    df_paths['nb_paths']=df_paths.apply(lambda x: x['paths'].count(x['start']), axis=1)\n"
                          "    df_paths['paths'] = df_paths.apply(lambda x: [value for value in x['paths'] if value not in [x['start'], x['end']]], axis=1)\n"
                          "    df=MatchEntityID(driver,EntityLabel)\n"
                          "    df['Betweenness']=df.apply(lambda m: df_paths.apply(lambda n: n['paths'].count(m['NodeId']/n['nb_paths']), axis=1).sum(), axis=1)\n"
                          "    df = df.drop('NodeId', axis=1)\n"
                          "    return df\n"
                          "\n"
                          "def BetweennessOnTimeline(driver, EntityLabel, RelationshipType=None):\n"
                          "\n"
                          "    Betweenness=[]\n"
                          "    # if there is no restriction on relationship type\n"
                          "    if RelationshipType is None:\n"
                          "        # calculate degree centrality for every entity in every unit\n"
                          "        for i in range(0,len(time_axis)):\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            result_betweenness = BetweennessInUnit(driver,EntityLabel, t, t_after)\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                result_betweenness.set_index('EntityId',inplace=True)\n"
                          "                result_betweenness.columns=[str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            # collect result\n"
                          "            Betweenness.append(result_betweenness)\n"
                          "    # if there is a selection on relationship type\n"
                          "    else:\n"
                          "        # unify the order of relationship names\n"
                          "        RelationshipType.sort()\n"
                          "        # transform them to string with the separator for Neo4j CQL\n"
                          "        RelationshipTypeStr='|'.join(RelationshipType)\n"
                          "\n"
                          "        for i in range(0,len(time_axis)):\n"
                          "            # calculate degree centrality for every entity in every unit\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            result_betweenness = BetweennessInUnit(driver,EntityLabel, t, t_after, RelationshipTypeStr)\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                result_betweenness.set_index('EntityId',inplace=True)\n"
                          "                result_betweenness.columns=[str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            Betweenness.append(result_betweenness)\n"
                          "    Betweenness_df=pd.concat(Betweenness, axis=1)\n"
                          "    Betweenness_df.fillna(0,inplace=True)\n"
                          "    return Betweenness_df\n"
                          "\n"
                          "def BetweennessCentrality_result_to_csv(EntityLabel,RelationshipType=None):\n"
                          "    # connect to database\n"
                          "    driver=conn()\n"
                          "    # calculate the betweenness centrality\n"
                          "    df=BetweennessOnTimeline(driver, EntityLabel, RelationshipType)\n"
                          f"    df.to_csv(csv_dir + '/betweenness{nb_machine}_{i + 1}.csv')\n"
                          "\n"
                          "def timing(EntityLabel,RelationshipType=None):\n"
                          "    time_start = time.time()\n"
                          "    BetweennessCentrality_result_to_csv(EntityLabel,RelationshipType=RelationshipType)\n"
                          "    time_end = time.time()\n"
                          "    time_c = time_end - time_start\n"
                          "    print('time cost', time_c, 's')\n"
                          "\n"
                          "# Change the working directory to the Neo4j installation directory\n"
                          "os.chdir(neo4j_dir)\n"
                          "\n"
                          "# Start Neo4j\n"
                          "os.system('./neo4j start')\n"
                          "\n"
                          "time.sleep(60)\n"
                          "\n"
                          f"timing(EntityLabel='Station')\n"
                          "\n"
                          "os.system('./neo4j stop')\n")

        os.chdir(script_dir)
        with open(f'betweenness_{nb_machine}_{str(i + 1)}.py', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to betweenness_{nb_machine}_{str(i + 1)}.py")


python = '/users/sig/song/env/bin/python'


def generate_sh(nb_machine, task_name, python, partition, nb_task, cpu_per_task):
    for i in range(nb_machine):
        generated_code = f"""#!/bin/bash

#SBATCH --job-name={task_name}{nb_machine}_{str(i + 1)}
#SBATCH --output={task_name}{nb_machine}_{str(i + 1)}.out
#SBATCH --error={task_name}{nb_machine}_{str(i + 1)}.er

#SBATCH --partition={partition}

#SBATCH --ntasks={nb_task}
#SBATCH --cpus-per-task={cpu_per_task} 

python={python}

script={script_dir}/{task_name}_{nb_machine}_{str(i + 1)}.py

srun ${{python}} ${{script}}
"""
        os.chdir(script_dir)
        with open(f'betweenness_{nb_machine}_{str(i + 1)}.sh', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to betweenness_{nb_machine}_{str(i + 1)}.sh")


nb_machine = int(sys.stdin.read())

clear_folder(script_dir)

generate_py(nb_machine, '2021-02-01T00:00:00', '2021-03-31T00:00:00', 'd')
generate_sh(nb_machine, 'betweenness', python, '24CPUNodes', '1', '24')
