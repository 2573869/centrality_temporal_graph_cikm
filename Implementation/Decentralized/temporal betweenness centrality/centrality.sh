#!/bin/bash

python=/users/sig/song/env/bin/python

script=create_scripts.py 

echo "10" | ${python} ${script}

shell_scripts=$(find /users/sig/song/Exp_centrality/Betweenness/balanced/script -name "*.sh")

job_ids=""

echo "Submitting distributed sub-graph jobs..."
for script in $shell_scripts; do
    job_id=$(sbatch $script | awk '{print $4}')
    echo "Submitted job $job_id"
    job_ids+=":$job_id"
done

echo "Submitting aggregation job..."
aggregation_job_id=$(sbatch --dependency=afterok${job_ids} aggregation.sh | awk '{print $4}')
echo "Submitted job $aggregation_job_id with dependency on $job_ids"

echo "All jobs submitted."

