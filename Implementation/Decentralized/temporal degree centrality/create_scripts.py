import pandas as pd
import math
import os
import sys
import numpy as np

def clear_folder(folder_path):
    # Iterate over all files and subdirectories in the folder
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
            # Remove each file
            file_path = os.path.join(root, name)
            os.remove(file_path)
        for name in dirs:
            # Remove each subdirectory
            dir_path = os.path.join(root, name)
            os.rmdir(dir_path)


script_dir = "/users/sig/song/Exp_centrality/Degree/script"
csv_dir = "/users/sig/song/Exp_centrality/Degree/csv"
EntityIdAttributeNameDB = "n.user_id"

def generate_py(nb_machine, IntervalStartUnit, IntervalEndUnit, TimeUnit):
    start_interval_datetime = pd.to_datetime(IntervalStartUnit)
    end_interval_datetime = pd.to_datetime(IntervalEndUnit)
    timeline = pd.date_range(start_interval_datetime, end_interval_datetime, freq=TimeUnit)
    timeline_list = timeline.tolist()
    for i in range(nb_machine):
        sub_tl = timeline_list[i * math.ceil(len(timeline) / nb_machine):min((i + 1) * math.ceil(len(timeline) / nb_machine), len(timeline))+1]
        sub_tl_str = sub_tl.__str__()
        generated_code = ("import neo4j\n"
                          "import pandas as pd\n"
                          "import time\n"
                          "import matplotlib.pyplot as plt\n"
                          "import datetime\n"
                          "import os\n"
                          "from pandas import Timestamp\n"
                          "from neo4j import GraphDatabase, RoutingControl\n"
                          "from neo4j.exceptions import DriverError, Neo4jError\n"
                          "\n"
                          "neo4j_url = \"bolt://localhost:7687\"\n"
                          "AUTH = ('neo4j', '0000')\n"
                          "# in advance, you have to manually change the active database with respect to the volume you want to query\n"
                          "# Specify the Neo4j installation directory\n"
                          f"neo4j_dir = \"/users/sig/song/neo4j_{str(i + 1)}/bin\"\n"
                          f"csv_dir = \"{csv_dir}\"\n"
                          "# dictionary for dataframes to reduce the time of calculation\n"
                          f"EntityIdAttributeNameDB='{EntityIdAttributeNameDB}'\n"
                          "# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset\n"
                          "\n"
                          "def conn():\n"
                          "    return GraphDatabase.driver(neo4j_url, auth=AUTH)\n"
                          "\n"
                          f"time_axis=pd.DatetimeIndex({sub_tl_str})\n"
                          "print(time_axis)\n"
                          "\n"
                          "def DegreeInUnit(driver, UnitStartTime, UnitEndTime, EntityLabel, RelationshipTypeStr=None):\n"
                          "    # query for degree in one unit, maybe with relationships chosen\n"
                          "    query_degree = f\"\"\"MATCH (n:{EntityLabel}){'-' + f\"[r:{RelationshipTypeStr}]\" if RelationshipTypeStr else \"-[r]\"}-() WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN {EntityIdAttributeNameDB} AS EntityId, count(r) AS DegreeEntity\"\"\"\n"
                          "    print(query_degree)\n"
                          "    return driver.execute_query(query_degree,result_transformer_ = neo4j.Result.to_df)\n\n"
                          "\n"
                          "def DegreeOnTimeline(driver, EntityLabel, RelationshipType=None):\n"
                          "    EntityDegree = []\n"
                          "    # if there is no restriction on relationship type\n"
                          "    if RelationshipType is None:\n"
                          "        # calculate degree centrality for every entity in every unit\n"
                          "        for i in range(0, len(time_axis)-1):\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            result_query_degree = DegreeInUnit(driver, t, t_after, EntityLabel)\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                result_query_degree.set_index('EntityId', inplace=True)\n"
                          "                result_query_degree.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            # collect result\n"
                          "            EntityDegree.append(result_query_degree)\n"
                          "    # if there is a selection on relationship type\n"
                          "    else:\n"
                          "        # unify the order of relationship names\n"
                          "        RelationshipType.sort()\n"
                          "        # transform them to string with the separator for Neo4j CQL\n"
                          "        RelationshipTypeStr = '|'.join(RelationshipType)\n\n"
                          "        for i in range(0, len(time_axis)-1):\n"
                          "            # calculate degree centrality for every entity in every unit\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            result_query_degree = DegreeInUnit(driver, t, t_after, EntityLabel, RelationshipTypeStr)\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                result_query_degree.set_index('EntityId', inplace=True)\n"
                          "                result_query_degree.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            EntityDegree.append(result_query_degree)\n"
                          "    EntityDegree_df = pd.concat(EntityDegree, axis=1)\n"
                          "    EntityDegree_df.fillna(0, inplace=True)\n" 
                          "    return EntityDegree_df\n"
                          "\n"
                          "def DegreeCentrality_to_csv(EntityLabel,RelationshipType=None):\n"
                          "    # connect to database\n"
                          "    driver=conn()\n"
                          "    # calculate the degree centrality\n"
                          "    df=DegreeOnTimeline(driver, EntityLabel, RelationshipType)\n"
                          f"    df.to_csv(csv_dir + '/degree{nb_machine}_{i + 1}.csv')\n"
                          "\n"
                          "def timing(EntityLabel, RelationshipType=None):\n"
                          "    time_start = time.time()\n"
                          "    DegreeCentrality_to_csv(EntityLabel,RelationshipType=RelationshipType)\n"
                          "    time_end = time.time()\n"
                          "    time_c = time_end - time_start\n"
                          "    print('time cost', time_c, 's')\n"
                          "\n"
                          "def import_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/Student.csv ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/WLAN.csv ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/State.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Access.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Call.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/MakeUp.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/SendSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Proximity.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/CloseFriend.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_snapshot_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Student.csv ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/WLAN.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Access.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Call.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SendSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Proximity.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/CloseFriend.csv ' \\\n"
                          "      '--relationships=//users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_e_commerce():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j --nodes=/users/sig/song/neo4j/import/user.csv --nodes=/users/sig/song/neo4j/import/category.csv --nodes=/users/sig/song/neo4j/import/item1_5.csv --nodes=/users/sig/song/neo4j/import/item6_10.csv --nodes=/users/sig/song/neo4j/import/item11_15.csv --nodes=/users/sig/song/neo4j/import/item16_20.csv --nodes=/users/sig/song/neo4j/import/item26_30.csv --nodes=/users/sig/song/neo4j/import/item31_34.csv --relationships=/users/sig/song/neo4j/import/addtocart.csv --relationships=/users/sig/song/neo4j/import/transaction.csv --relationships=/users/sig/song/neo4j/import/view.csv --relationships=/users/sig/song/neo4j/import/belongto.csv --relationships=/users/sig/song/neo4j/import/subCategory.csv --skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_city_bike():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "              '--nodes=/users/sig/song/neo4j/import/station.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202102_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/2020_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202101_trip.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202103_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202104_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202105_trip.csv ' \\\n"
                          "              '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "# Change the working directory to the Neo4j installation directory\n"
                          "os.chdir(neo4j_dir)\n"
                          "\n"
                          "import_social_experiment()\n"
                          "#import_e_commerce()\n"
                          "# Start Neo4j\n"
                          "os.system('./neo4j start')\n"
                          "\n"
                          "time.sleep(120)\n"
                          "\n"
                          f"timing(EntityLabel='Student')\n"
                          "\n"
                          "os.system('./neo4j stop')\n")

        os.chdir(script_dir)
        with open(f'degree_{nb_machine}_{str(i + 1)}.py', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to degree_{nb_machine}_{str(i + 1)}.py")


python = '/users/sig/song/env/bin/python'


def generate_sh(nb_machine, task_name, python, partition, nb_task, cpu_per_task):
    for i in range(nb_machine):
        generated_code = f"""#!/bin/bash

#SBATCH --job-name={task_name}{nb_machine}_{str(i + 1)}
#SBATCH --output={task_name}{nb_machine}_{str(i + 1)}.out
#SBATCH --error={task_name}{nb_machine}_{str(i + 1)}.er

#SBATCH --partition={partition}

#SBATCH --ntasks={nb_task}
#SBATCH --cpus-per-task={cpu_per_task} 

python={python}

script={script_dir}/{task_name}_{nb_machine}_{str(i + 1)}.py

srun ${{python}} ${{script}}
"""
        os.chdir(script_dir)
        with open(f'degree_{nb_machine}_{str(i + 1)}.sh', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to degree_{nb_machine}_{str(i + 1)}.sh")


nb_machine = int(sys.stdin.read())

clear_folder(script_dir)

generate_py(nb_machine, '2009-01-09T00:00:00', '2009-04-25T00:00:00', 'd')
generate_sh(nb_machine, 'degree', python, '48CPUNodes', '1', '16')
