import os
import random
import EigenvectorCentrality
import time
import configparser
import os



#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)

password_admin = config['admin']['password']
dataset_source_path = config['paths']['dataset_source_path']
dataset_target_path = config['paths']['dataset_target_path']

# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.user_id'
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset
TOTAL_NUM_RELS = 2168270

def start_db():
    # start server
    cmd = 'sudo neo4j start'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def stop_db():
    # stop server
    cmd = 'sudo neo4j stop'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def enter_folder_neo4j():
    # change the work space of cmd to the folder neo4j
    cmd = 'cd /var/lib/neo4j/'
    os.system(cmd)

def import_social_experiment(database_name):
    # import data
    cmd = f'sudo neo4j-admin database import full --overwrite-destination --nodes=/var/lib/neo4j/import/Student.csv  --nodes=/var/lib/neo4j/import/WLAN.csv --relationships=/var/lib/neo4j/import/Access.csv --relationships=/var/lib/neo4j/import/BlogLivejournalTwitter.csv --relationships=/var/lib/neo4j/import/Call.csv --relationships=/var/lib/neo4j/import/CloseFriend.csv --relationships=/var/lib/neo4j/import/FacebookAllTaggedPhotos.csv --relationships=/var/lib/neo4j/import/PoliticalDiscussant.csv --relationships=/var/lib/neo4j/import/Proximity.csv --relationships=/var/lib/neo4j/import/ReceiveSMS.csv --relationships=/var/lib/neo4j/import/SendSMS.csv --relationships=/var/lib/neo4j/import/SocializeTwicePerWeek.csv  --skip-bad-relationships --skip-duplicate-nodes {database_name}'

    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

#def remove_db(database_name) :
#    start_db()
#    driver = GraphDatabase.driver(neo4j_url, auth=(username_neo4j, password_neo4j))

    # Execute the "DROP DATABASE" Cypher command
 #   with driver.session() as session:
  #      session.run(f"DROP DATABASE {database_name}")

    # Close the driver connection
   # driver.close()
def remove_db(database_name) :
    cmd='cd /var/lib/neo4j/data/databases'
    os.system(cmd)
    cmd=f'sudo rm -rf {database_name}'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def drop_relationships_by_percentage(graph, percentage_left):
    # Calculate the number of relationships to delete based on the percentage
    num_to_delete = int(TOTAL_NUM_RELS * (1 - percentage_left / 100))

    # Generate a list of ids to delete
    ids_to_delete = random.sample(range(TOTAL_NUM_RELS), num_to_delete)
    #ids_to_delete_separated=chunks(ids_to_delete,3)
    #for i in range(0,3):
        # Delete the selected relationships
        #query = f"MATCH ()-[r]-() WHERE id(r) in {str(ids_to_delete_separated[i])} DELETE r"
    query = f"MATCH ()-[r]-() WHERE id(r) in {str(ids_to_delete)} DELETE r"
    graph.run(query)

def test(time_cost,database_name,percentage_list,EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    try:
        stop_db()
    except:
        None
    for p in percentage_list :
        l=[]
        for i in range(0,10) :
            enter_folder_neo4j()
            import_social_experiment(database_name)
            start_db()
            time.sleep(30)
            graph = EigenvectorCentrality.conn()
            drop_relationships_by_percentage(graph, p)
            l.append(EigenvectorCentrality.timing(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution))
            stop_db()
        print(l)
        time_cost[p/100] = l
    return time_cost