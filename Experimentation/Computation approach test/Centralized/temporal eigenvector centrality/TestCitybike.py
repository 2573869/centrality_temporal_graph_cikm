import os
import random
import EigenvectorCentrality
import time

import configparser
import os



#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)

password_admin = config['admin']['password']
dataset_source_path = config['paths']['dataset_source_path']
dataset_target_path = config['paths']['dataset_target_path']

# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.stationid' #{'n.itemid, n.categoryid, n.userid'}
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset
TOTAL_NUM_RELS =2181077

def start_db():
    # start server
    cmd = 'sudo neo4j start'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def stop_db():
    # stop server
    cmd = 'sudo neo4j stop'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def enter_folder_neo4j():
    # change the work space of cmd to the folder neo4j
    cmd = 'cd /var/lib/neo4j/'
    os.system(cmd)

def import_citibike(database_name):
    # import data
    cmd = f'sudo neo4j-admin database import full --overwrite-destination --nodes=/var/lib/neo4j/import/station.csv ' \
            '--relationships=/var/lib/neo4j/import/202102_trip.csv ' \
            '--relationships=/var/lib/neo4j/import/202103_trip.csv ' \
          f'--skip-bad-relationships --skip-duplicate-nodes {database_name}'

    os.system('echo %s|sudo -S %s' % (password_admin, cmd))



def remove_db(database_name) :
    cmd='cd /var/lib/neo4j/data/databases'
    os.system(cmd)
    cmd=f'sudo rm -rf {database_name}'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def split_list(lst, size):
    """
    Splits a given list into sub-lists of a given size.

    Args:
    lst (list): The list to be split.
    size (int): The size of each sub-list.

    Returns:
    list: A list of sub-lists.
    """
    return [lst[i:i+size] for i in range(0, len(lst), size)]


def drop_relationships_by_percentage(graph, percentage_left):
    # Calculate the number of relationships to delete based on the percentage
    num_to_delete = int(TOTAL_NUM_RELS * (1 - percentage_left / 100))
    print(num_to_delete)

    # Generate a list of ids to delete
    ids_to_delete = random.sample(range(TOTAL_NUM_RELS), num_to_delete)
    ids_to_delete_split=split_list(ids_to_delete, round(percentage_left/20))
    #ids_to_delete_split=split_list(ids_to_delete, 5)
    for l in ids_to_delete_split:
        query = f"MATCH ()-[r]-() WHERE id(r) in {str(l)} DELETE r"
        graph.run(query)
        # clear the query cache
        graph.run('CALL db.clearQueryCaches()')

    print(f'drop {str(100-percentage_left)}% relashionships from dataset')

def clear_cache():
    cmd = 'sudo sync && sudo sysctl -w vm.drop_caches=3'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def relation_number(graph):
    query = "CALL apoc.meta.stats() yield relCount RETURN relCount"
    r=graph.run(query)
    print(r)

def test(time_cost,database_name,percentage_list,EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    try:
        stop_db()
    except:
        None
    for p in percentage_list :
        l=[]
        for i in range(0,10):
            enter_folder_neo4j()
            import_citibike(database_name)
            start_db()
            time.sleep(60)
            graph = EigenvectorCentrality.conn()
            print('Successfully connected!')
            drop_relationships_by_percentage(graph, p)
            #relation_number(graph)
            l.append(EigenvectorCentrality.timing(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution))
            stop_db()
            clear_cache()
        time_cost[p/100] = l
        print(l)