import DegreeCentrality
import TestSocialExperiment
import TestECommerce
import TestCitybike
import DegreeCentrality_timeline_by_relationship
import time
import TestECommerce

###
# example usage
###
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset
time_cost = {}
p_l=[100,80,60,40,20]
#p_l=[99]
#TestSocialExperiment.test(time_cost,'social.db', p_l,'Student','d',RelationshipType=['CloseFriend'],show_distribution=False)
#TestSocialExperiment.test(time_cost,'social.db', p_l,'Student','d',show_distribution=False)
#TestECommerce.test(time_cost, 'e-commerce.db', p_l, 'Item', 'd', RelationshipType=['Transaction'],show_distribution=False)
TestECommerce.test(time_cost, 'e-commerce.db', p_l, 'Item', 'd',show_distribution=False)
#TestCitybike.test(time_cost, 'citybike.db', p_l, 'Station', 'd', show_distribution=False)
print(time_cost)

#DegreeCentrality.timing('Student','d',RelationshipType=['SendSMS'],show_distribution=False)
#DegreeCentrality.DegreeCentrality('Student','d',RelationshipType=['SendSMS'])
#TestSocialExperiment.import_social_experiment('social.db')
#TestSocialExperiment.stop_db()
#TestECommerce.import_e_commerce('e-commerce.db')
#TestSocialExperiment.start_db()
#time.sleep(60)
#graph = DegreeCentrality_timeline_by_relationship.conn()
#print('Successfully connected!')
#TestECommerce.drop_relationships_by_percentage(graph, 80)
#DegreeCentrality.timing('Student','d',EntityIdentifier='24',RelationshipType=['SendSMS'])
#DegreeCentrality.timing('Student','d',EntityIdentifier='5')
#DegreeCentrality.timing('Student','d')
#DegreeCentrality.timing('Student','d',show_distribution=False)
#DegreeCentrality.timing('Student','d',EntityIdentifier='24')
#DegreeCentrality_timeline_by_relationship.timing('Item','d',RelationshipType=['Transaction'],show_distribution=False)
#DegreeCentrality_timeline_by_relationship.timing('Item','d',RelationshipType=['Transaction'],EntityIdentifier='173653')
#DegreeCentrality_timeline_by_relationship.timing('Item','d',show_distribution=False)
#DegreeCentrality_timeline_by_relationship.timing('Item','d')
#DegreeCentrality_timeline_by_relationship.timing('Item','d',EntityIdentifier='173653')

#TestSocialExperiment.stop_db()
#TestECommerce.import_e_commerce('e-commerce.db')
#TestECommerce.start_db()
#time.sleep(5)
#graph = DegreeCentrality_timeline_by_relationship.conn()
#print('Successfully connected!')
#TestECommerce.drop_relationships_by_percentage(graph, 80)
#DegreeCentrality_timeline_by_relationship.timing('Item','d',show_distribution=False)
