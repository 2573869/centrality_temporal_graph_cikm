import pandas as pd
import py2neo as pn
import time
import matplotlib
#matplotlib.use('Qt5Agg') # replace 'Qt5Agg' with your desired backend
import matplotlib.pyplot as plt
import datetime

import configparser
import os

#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)
# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']

# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.itemid'
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset

def conn():
    # connect DB
    try:
        return pn.Graph(neo4j_url,auth=(username_neo4j, password_neo4j))
    except Exception as error:
        print('Caught this error: ' + repr(error))

def GetTimeline(graph):
    # query for timeline of entities with given label
    query_max_min = "MATCH ()-[r]-()" +\
                " RETURN min(r.startvalidtime) as min, max(r.endvalidtime) as max"
    return graph.run(query_max_min).to_data_frame()

def SplitTimeline(result_min_max,TimeUnit):
    tmin = result_min_max["min"][0]
    tmax = result_min_max["max"][0]
    # split the timeline with given time unit
    return pd.date_range(tmin, tmax, freq=TimeUnit)

def DegreeInUnit(graph, UnitStartTime, UnitEndTime, EntityLabel, RelationshipTypeStr=None):
    # query for degree in one unit, maybe with relationships chosen
    query_degree = f"""MATCH (n:{EntityLabel}){"-" + f"[r:{RelationshipTypeStr}]" if RelationshipTypeStr else "-[r]"}-() WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN {EntityIdAttributeNameDB} AS EntityId, count(r) AS DegreeEntity"""
    print(query_degree)
    return graph.run(query_degree).to_data_frame()

def DegreeOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit = None, IntervalEndUnit = None, RelationshipType=None):

    # create the list of time units on the timeline
    if (IntervalStartUnit is None) & (IntervalEndUnit is None):
        time_axis = SplitTimeline(GetTimeline(graph),TimeUnit)
    else :
        time_axis = pd.date_range(IntervalStartUnit, IntervalEndUnit, freq=TimeUnit)

    EntityDegree=[]
    # if there is no restriction on relationship type
    if RelationshipType is None:
        # calculate degree centrality for every entity in every unit
        for i in range(0,len(time_axis)-1):
            t = datetime.datetime.isoformat(time_axis[i])
            #try:
                # try to find required dataframe from the dictionary
            #    result_query_degree=data[f'{EntityLabel}_{TimeUnit}'].loc[:,str(t)]
            #except:
            t_after = datetime.datetime.isoformat(time_axis[i+1])
            result_query_degree = DegreeInUnit(graph, t, t_after, EntityLabel)
                 # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_query_degree.set_index('EntityId',inplace=True)
                result_query_degree.columns=[str(t)]
            except:
                continue
            try:
                data[f'{EntityLabel}_{TimeUnit}'] = pd.concat([data[f'{EntityLabel}_{TimeUnit}'], result_query_degree], axis=1)
            except:
                data[f'{EntityLabel}_{TimeUnit}'] = result_query_degree
            # collect result
            EntityDegree.append(result_query_degree)
    # if there is a selection on relationship type
    else:
        # unify the order of relationship names
        RelationshipType.sort()
        # transform them to string with the separator for Neo4j CQL
        RelationshipTypeStr='|'.join(RelationshipType)

        for i in range(0,len(time_axis)-1):
            # calculate degree centrality for every entity in every unit
            t = datetime.datetime.isoformat(time_axis[i])
            #try:
                # try to find required dataframe from the dictionary
            #    result_query_degree=data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'].loc[:,str(t)]
            #except:
            t_after = datetime.datetime.isoformat(time_axis[i+1])
            result_query_degree = DegreeInUnit(graph, t, t_after, EntityLabel, RelationshipTypeStr)
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_query_degree.set_index('EntityId',inplace=True)
                result_query_degree.columns=[str(t)]
            except:
                continue
            try:
                data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'] = pd.concat([data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'], result_query_degree], axis=1)
            except:
                data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'] = result_query_degree
            EntityDegree.append(result_query_degree)
    return pd.concat(EntityDegree, axis=1)

def DegreeOfInterval(DataFrame,IntervalStartUnit,IntervalEndUnit,EntityIdentifier):
    # calculate degree centrality of one entity in a time interval chosen
    return DataFrame.loc[EntityIdentifier,IntervalStartUnit:IntervalEndUnit].mean()

def Position(EntitiesDegree, EntityIdentifier=None, show_distribution=True):
    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show degree centrality of every entity with the label chosen and their distribution
        EntitiesDegree.columns=['Degree']
        EntitiesDegree=EntitiesDegree.sort_values(by='Degree',ascending=False)
        EntitiesDegree['Rank']=range(1,EntitiesDegree.shape[0]+1)
        if show_distribution :
            EntitiesDegree['Degree'].plot(kind='box')
            EntitiesDegree['Position']=EntitiesDegree['Rank']/EntitiesDegree.shape[0]
        print(EntitiesDegree)
    # if it is not for a special time interval
    else:
        # show degree centrality of chosen entity with its rank and position among all entities with the same label
        EntitiesDegree.columns=['Degree']
        EntitiesDegree=EntitiesDegree.sort_values(by='Degree',ascending=False)
        EntitiesDegree['Rank']=range(1,EntitiesDegree.shape[0]+1)
        if show_distribution :
            EntitiesDegree['Degree'].plot(kind='box')
            plt.axhline(y=EntitiesDegree.loc[EntityIdentifier].iloc[0],ls='--',label='Entity')
            plt.legend()
            plt.show(block=False)
            EntitiesDegree['Position']=EntitiesDegree['Rank']/EntitiesDegree.shape[0]
        print(EntitiesDegree.loc[EntityIdentifier])

def Evolution(DegreeInEveryUnit, EntityIdentifier):
    # show degree evolution of an entity in its valid time
    DegreeInEveryUnit.loc[EntityIdentifier].plot()

def DegreeCentrality(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Calculates the degree centrality of entities over time and returns the results in various formats.

    Parameters:
    EntityLabel (str): The label of the entity to analyze.
    TimeUnit (str): The time unit to use for analysis (e.g. 'm' for month, 'w' for week, 'd' for day).
    RelationshipType (list of str, optional): The types of relationships to consider in analysis. If not specified, all relationship types will be considered.
    EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
    IntervalStartUnit (str, optional): The start time unit of a time interval to analyze (inclusive).
    IntervalEndUnit (str, optional): The end time unit of a time interval to analyze (inclusive).
    show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
    None
    """

    # connect to database
    graph=conn()

    df=DegreeOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit=IntervalStartUnit, IntervalEndUnit=IntervalEndUnit, RelationshipType=RelationshipType)
    #graph.node_cache.clear()
    #graph.relationship_cache.clear()
    #graph.delete_all()

    # clear the query cache
    graph.run('CALL db.clearQueryCaches()')

    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show the distribution curve and degree table
        Position(pd.DataFrame(df.mean(axis=1)),show_distribution=show_distribution)
    # if it is not for a special time interval
    else :
        # show degree evolution of an entity in its valid time
        Evolution(df, EntityIdentifier)
        # show the distribution curve and the position of the entity
        Position(pd.DataFrame(df.mean(axis=1)),EntityIdentifier,show_distribution=show_distribution)

def timing(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    time_start = time.time()
    DegreeCentrality(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution)
    time_end = time.time()
    time_c = time_end - time_start
    print('time cost', time_c, 's')
    return time_c


