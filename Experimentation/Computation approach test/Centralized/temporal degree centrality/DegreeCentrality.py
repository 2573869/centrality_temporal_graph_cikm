import pandas as pd
import py2neo as pn
import time
import matplotlib.pyplot as plt
import datetime
import configparser
import os

#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)
# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB = config['database']['EntityIdAttributeNameDB']
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset

def conn():
    """
    Connect to Neo4j

    Parameters: None

    Returns: py2neo.database.Graph: A Py2neo Graph object representing the connection to the Neo4j graph database
    """
    try:
    # connect DB
        return pn.Graph(neo4j_url,auth=(username_neo4j, password_neo4j))
    except Exception as error:
        print('Caught this error: ' + repr(error))

def GetTimeline(graph,EntityLabel):
    """
    Retrieve the start and end times for a timeline of entities with a given label.

    Parameters:
    graph (py2neo.database.Graph): A Py2neo Graph object representing the connection to the Neo4j graph database.
    EntityLabel (str): A string that specifies the label of the entities for which the timeline is to be retrieved.

    Returns:
    pandas.DataFrame: A Pandas DataFrame with two columns, "min" and "max", which contain the minimum start time and maximum end time, respectively, of the entities with the specified label in the graph database.
    """
    # query for timeline of entities with given label

    # query for timeline of entities with given label
    query_max_min = f"MATCH (n: {EntityLabel})" +\
                " RETURN min(n.startvalidtime) as min, max(n.endvalidtime) as max"
    return graph.run(query_max_min).to_data_frame()

def SplitTimeline(TimeUnit, result_min_max=None, IntervalStartUnit=None, IntervalEndUnit=None):
    """
    Splits a timeline into intervals of a given time unit.

    Parameters:
    TimeUnit (str): A string that specifies the time unit used for splitting the timeline, e.g. "D" for days, "M" for months, etc.
    result_min_max (pandas.DataFrame, optional): A Pandas DataFrame with two columns, "min" and "max", which contain the minimum start time and maximum end time, respectively, of the timeline. If not provided, this function will assume that the entire timeline should be split.
    IntervalStartUnit (str, optional): A string that specifies the start time of the interval to be split. If provided, this function will split the interval between this time and the time specified in IntervalEndUnit.
    IntervalEndUnit (str, optional): A string that specifies the end time of the interval to be split. If provided, this function will split the interval between this time and the time specified in IntervalStartUnit.

    Returns:
    pandas.DatetimeIndex: A DatetimeIndex object containing the start times of each interval in the timeline, split according to the specified time unit.
    """
    if (IntervalStartUnit is None) & (IntervalEndUnit is None):
        tmin = result_min_max["min"][0]
        tmax = result_min_max["max"][0]
        # split the timeline with given time unit
        return pd.date_range(tmin, tmax, freq=TimeUnit)
    else:
        start_interval_datetime = pd.to_datetime(IntervalStartUnit)
        end_interval_datetime = pd.to_datetime(IntervalEndUnit)
        end_interval_end_datetime = end_interval_datetime + pd.Timedelta(days=1)
        return pd.date_range(start_interval_datetime, end_interval_end_datetime, freq=TimeUnit)
#%%

def DegreeInUnit(graph, UnitStartTime, UnitEndTime, EntityLabel, RelationshipTypeStr=None):
    """
    Calculate the sum of relationships with given relationship type (if specified)existent within the given time unit as the degree of entities with the given label.

    Parameters:
    - graph: The Neo4j graph object on which the query is run.
    - UnitStartTime: The start time of the unit for which the degree is to be calculated.
    - UnitEndTime: The end time of the unit for which the degree is to be calculated.
    - EntityLabel: The label of the entity for which the degree is to be calculated.
    - RelationshipTypeStr (optional): The type of relationship between the entities. If not provided, all relationships are considered.

    Returns:
    - A Pandas DataFrame object containing the EntityId and DegreeEntity columns, where EntityId is the ID of the entity and DegreeEntity is the degree of the entity in the given time unit.
    """
    # query for degree in one unit, maybe with relationships chosen
    query_degree = f"""MATCH (n:{EntityLabel}){"-" + f"[r:{RelationshipTypeStr}]" if RelationshipTypeStr else "-[r]"}-() WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN {EntityIdAttributeNameDB} AS EntityId, count(r) AS DegreeEntity"""
    print(query_degree)
    return graph.run(query_degree).to_data_frame()

def DegreeOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit = None, IntervalEndUnit = None, RelationshipType=None):
    """
    Get the degree of each entity with the given label over all time units on the timeline.

    Parameters:
        graph (py2neo.database.Graph): Graph database object to query.
        EntityLabel (str): Label of the entity for which degree is calculated.
        TimeUnit (str): Time unit for which the degree is calculated (e.g. "D" for day, "M" for month).
        IntervalStartUnit (str, optional): Start of the time interval for which the degree is calculated.
        IntervalEndUnit (str, optional): End of the time interval for which the degree is calculated.
        RelationshipType (list, optional): Types of relationships to include in the degree calculation. If None, all relationships are included.

    Returns:
        pd.DataFrame: A DataFrame with the degree of each entity with the given label over time.
    """
    # create the list of time units on the timeline
    if (IntervalStartUnit is None) & (IntervalEndUnit is None):
        time_axis = SplitTimeline(GetTimeline(graph, EntityLabel),TimeUnit)
    else :
        time_axis = pd.date_range(IntervalStartUnit, IntervalEndUnit, freq=TimeUnit)

    EntityDegree=[]
    # if there is no restriction on relationship type
    if RelationshipType is None:
        # calculate degree centrality for every entity in every unit
        for i in range(0,len(time_axis)-1):
            t = datetime.datetime.isoformat(time_axis[i])
            #try:
                # try to find required dataframe from the dictionary
            #    result_query_degree=data[f'{EntityLabel}_{TimeUnit}'].loc[:,str(t)]
            #except:
            t_after = datetime.datetime.isoformat(time_axis[i+1])
            result_query_degree = DegreeInUnit(graph, t, t_after, EntityLabel)
                 # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_query_degree.set_index('EntityId',inplace=True)
                result_query_degree.columns=[str(t)]
            except:
                continue
            try:
                data[f'{EntityLabel}_{TimeUnit}'] = pd.concat([data[f'{EntityLabel}_{TimeUnit}'], result_query_degree], axis=1)
            except:
                data[f'{EntityLabel}_{TimeUnit}'] = result_query_degree
            # collect result
            EntityDegree.append(result_query_degree)
    # if there is a selection on relationship type
    else:
        # unify the order of relationship names
        RelationshipType.sort()
        # transform them to string with the separator for Neo4j CQL
        RelationshipTypeStr='|'.join(RelationshipType)

        for i in range(0,len(time_axis)-1):
            # calculate degree centrality for every entity in every unit
            t = datetime.datetime.isoformat(time_axis[i])
            #try:
                # try to find required dataframe from the dictionary
            #    result_query_degree=data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'].loc[:,str(t)]
            #except:
            t_after = datetime.datetime.isoformat(time_axis[i+1])
            result_query_degree = DegreeInUnit(graph, t, t_after, EntityLabel, RelationshipTypeStr)
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_query_degree.set_index('EntityId',inplace=True)
                result_query_degree.columns=[str(t)]
            except:
                continue
            try:
                data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'] = pd.concat([data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'], result_query_degree], axis=1)
            except:
                data[f'{EntityLabel}_{TimeUnit}_{RelationshipTypeStr}'] = result_query_degree
            EntityDegree.append(result_query_degree)
    return pd.concat(EntityDegree, axis=1)

def Position(EntitiesDegree, EntityIdentifier=None, show_distribution=True):
    """
    Sort the temporal degree centrality of all entities with the same label and show their distribution if required.

    Parameters:
        EntitiesDegree (pd.DataFrame): A DataFrame with the temporal degree of each entity with the given label.
        EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
        show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
        None
    """
    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show degree centrality of every entity with the label chosen and their distribution
        EntitiesDegree.columns=['Degree']
        EntitiesDegree=EntitiesDegree.sort_values(by='Degree',ascending=False)
        EntitiesDegree['Rank']=range(1,EntitiesDegree.shape[0]+1)
        if show_distribution :
            EntitiesDegree['Degree'].plot(kind='box')
            EntitiesDegree['Position']=EntitiesDegree['Rank']/EntitiesDegree.shape[0]
        print(EntitiesDegree)
    # if it is not for a special time interval
    else:
        # show degree centrality of chosen entity with its rank and position among all entities with the same label
        EntitiesDegree.columns=['Degree']
        EntitiesDegree=EntitiesDegree.sort_values(by='Degree',ascending=False)
        EntitiesDegree['Rank']=range(1,EntitiesDegree.shape[0]+1)
        if show_distribution :
            EntitiesDegree['Degree'].plot(kind='box')
            plt.axhline(y=EntitiesDegree.loc[EntityIdentifier].iloc[0],ls='--',label='Entity')
            plt.legend()
            plt.show(block=False)
            EntitiesDegree['Position']=EntitiesDegree['Rank']/EntitiesDegree.shape[0]
        print(EntitiesDegree.loc[EntityIdentifier])

def Evolution(DegreeInEveryUnit, EntityIdentifier):
    """
    Plots the degree evolution of an entity over time.

    Parameters:
        DegreeInEveryUnit (pd.DataFrame): A DataFrame with the temporal degree of each entity.
        EntityIdentifier (str): The identifier of the entity for which to plot the degree evolution.

    Returns:
        None
    """
    # show degree evolution of an entity in its valid time
    DegreeInEveryUnit.loc[EntityIdentifier].plot()

def DegreeCentrality(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Calculates the degree centrality of entities over time and returns the results in various formats.

    Parameters:
    EntityLabel (str): The label of the entity to analyze.
    TimeUnit (str): The time unit to use for analysis (e.g. 'm' for month, 'w' for week, 'd' for day).
    RelationshipType (list of str, optional): The types of relationships to consider in analysis. If not specified, all relationship types will be considered.
    EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
    IntervalStartUnit (str, optional): The start time unit of a time interval to analyze (inclusive).
    IntervalEndUnit (str, optional): The end time unit of a time interval to analyze (inclusive).
    show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
    None
    """

    # connect to database
    graph=conn()

    df=DegreeOnTimeline(graph, EntityLabel, TimeUnit, IntervalStartUnit=IntervalStartUnit, IntervalEndUnit=IntervalEndUnit, RelationshipType=RelationshipType)

    # clear the query cache
    graph.run('CALL db.clearQueryCaches()')

    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show the distribution curve and degree table
        Position(pd.DataFrame(df.mean(axis=1)),show_distribution=show_distribution)
    # if it is not for a special time interval
    else :
        # show degree evolution of an entity in its valid time
        Evolution(df, EntityIdentifier)
        # show the distribution curve and the position of the entity
        Position(pd.DataFrame(df.mean(axis=1)),EntityIdentifier,show_distribution=show_distribution)

def timing(EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Call the function DegreeCentrality to calculate the running time, parameters same as DegreeCentrality.

    Parameters:
    EntityLabel (str): The label of the entity to analyze.
    TimeUnit (str): The time unit to use for analysis (e.g. 'm' for month, 'w' for week, 'd' for day).
    RelationshipType (list of str, optional): The types of relationships to consider in analysis. If not specified, all relationship types will be considered.
    EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
    IntervalStartUnit (str, optional): The start time unit of a time interval to analyze (inclusive).
    IntervalEndUnit (str, optional): The end time unit of a time interval to analyze (inclusive).
    show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
    None
    """
    time_start = time.time()
    DegreeCentrality(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution)
    time_end = time.time()
    time_c = time_end - time_start
    print('time cost', time_c, 's')
    return time_c


