import os
import random
import DegreeCentrality_timeline_by_relationship
import time
import configparser
import os
from ImportDataset import import_e_commerce



#/!\ modify config.ini file 
current_directory = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(current_directory, 'config.ini')

print("Config file path:", config_file_path)  # Debug print

config = configparser.ConfigParser()
config.read(config_file_path)

password_admin = config['admin']['password']
# Set up the connection to the Neo4j database

neo4j_url = config['neo4j']['url']
username_neo4j = config['neo4j']['username']
password_neo4j = config['neo4j']['password']
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
data={}
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.itemid' #{'n.itemid, n.categoryid, n.userid'}
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset
TOTAL_NUM_RELS =4447430

def start_db():
    """
    Starts the Neo4j database server.

    Parameters:None

    Returns: None
    """

    # start server
    cmd = 'sudo neo4j start'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def stop_db():
    """
    Stops the Neo4j database server.

    Parameters: None

    Returns: None
    """
    # stop server
    cmd = 'sudo neo4j stop'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def enter_folder_neo4j():
    """
    Changes the working directory to the Neo4j folder.

    Parameters: None

    Returns: None
    """
    # change the work space of cmd to the folder neo4j
    cmd = 'cd /var/lib/neo4j/'
    os.system(cmd)

def import_e_commerce(database_name):
    """
    Imports data into the Neo4j database.

    Parameters:
        database_name (str): The name of the database to import data into.

    Returns:
        None
    """
    # import data
    cmd = f'sudo neo4j-admin database import full --overwrite-destination --nodes=/var/lib/neo4j/import/user.csv ' \
          '--nodes=/var/lib/neo4j/import/category.csv ' \
          '--nodes=/var/lib/neo4j/import/item1_5.csv ' \
          '--nodes=/var/lib/neo4j/import/item6_10.csv ' \
          '--nodes=/var/lib/neo4j/import/item11_15.csv ' \
          '--nodes=/var/lib/neo4j/import/item16_20.csv ' \
          '--nodes=/var/lib/neo4j/import/item26_30.csv ' \
          '--nodes=/var/lib/neo4j/import/item31_34.csv ' \
          '--relationships=/var/lib/neo4j/import/addtocart.csv ' \
          '--relationships=/var/lib/neo4j/import/transaction.csv ' \
          '--relationships=/var/lib/neo4j/import/view.csv ' \
          '--relationships=/var/lib/neo4j/import/belongto.csv ' \
          '--relationships=/var/lib/neo4j/import/subCategory.csv ' \
          f'--skip-bad-relationships --skip-duplicate-nodes {database_name}'

    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def split_list(lst, size):
    """
    Splits a given list into sub-lists of a given size.

    Args:
    lst (list): The list to be split.
    size (int): The size of each sub-list.

    Returns:
    list: A list of sub-lists.
    """
    return [lst[i:i+size] for i in range(0, len(lst), size)]


def drop_relationships_by_percentage(graph, percentage_left):
    """
    Deletes a specified percentage of relationships from a Neo4j database.

    Parameters:
        graph: A connection to the Neo4j database.
        percentage_left (int): The percentage of relationships to keep.

    Returns:
        None
    """
    # Calculate the number of relationships to delete based on the percentage
    num_to_delete = int(TOTAL_NUM_RELS * (1 - percentage_left / 100))
    print(num_to_delete)

    # Generate a list of ids to delete
    ids_to_delete = random.sample(range(TOTAL_NUM_RELS), num_to_delete)
    ids_to_delete_split=split_list(ids_to_delete, round(percentage_left/20))
    for l in ids_to_delete_split:
        query = f"MATCH ()-[r]-() WHERE id(r) in {str(l)} DELETE r"
        graph.run(query)
        # clear the query cache
        graph.run('CALL db.clearQueryCaches()')

    print(f'drop {str(100-percentage_left)}% relashionships from dataset')

def clear_cache():
    """
    Clears the file system cache.

    Parameters:
        None

    Returns:
        None
    """
    cmd = 'sudo sync && sudo sysctl -w vm.drop_caches=3'
    os.system('echo %s|sudo -S %s' % (password_admin, cmd))

def relation_number(graph):
    query = "CALL apoc.meta.stats() yield relCount RETURN relCount"
    r=graph.run(query)
    print(r)

def test(time_cost,database_name,percentage_list,EntityLabel,TimeUnit,IntervalStartUnit=None,IntervalEndUnit=None,RelationshipType=None,EntityIdentifier=None,show_distribution=True):
    """
    Tests the performance of the DegreeCentrality function on a Neo4j database by dropping a specified percentage of relationships.

    Parameters:
        time_cost (dict): A dictionary to store the results of the performance tests.
        database_name (str): The name of the Neo4j database to use for the tests.
        percentage_list (list[int]): A list of percentages of relationships to drop.
        EntityLabel (str): The label of the entities for which to compute the degree centrality.
        TimeUnit (str): The time unit to use for computing the degree centrality.
        IntervalStartUnit (str, optional): The start of the time interval to use for computing the degree centrality.
        IntervalEndUnit (str, optional): The end of the time interval to use for computing the degree centrality.
        RelationshipType (str, optional): The type of relationships to use for computing the degree centrality.
        EntityIdentifier (str, optional): The identifier of the entity for which to display the degree centrality.
        show_distribution (bool, optional): Whether to display the degree distribution plot.

    Returns:
        A dictionary with the results of the performance tests.
    """
    try:
        stop_db()
    except:
        None
    for p in percentage_list :
        l=[]
        for i in range(0,5) :
            enter_folder_neo4j()
            import_e_commerce(database_name)
            start_db()
            time.sleep(5)
            graph = DegreeCentrality_timeline_by_relationship.conn()
            print('Successfully connected!')
            drop_relationships_by_percentage(graph, p)
            #relation_number(graph)
            l.append(DegreeCentrality_timeline_by_relationship.timing(EntityLabel,TimeUnit,IntervalStartUnit=IntervalStartUnit,IntervalEndUnit=IntervalEndUnit,RelationshipType=RelationshipType,EntityIdentifier=EntityIdentifier,show_distribution=show_distribution))
            stop_db()
            clear_cache()
        time_cost[p/100] = l
        print(l)