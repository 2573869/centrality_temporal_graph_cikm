import pandas as pd
import os

"""
Python script to transfer temporal graph data to snapshot
"""

# fonction to split entities by day 
def split_entities(csv_file, snapshot_folder, id_mapping_folder):
    # Read CSV file into a DataFrame
    df = pd.read_csv(csv_file,index_col=None)

    # Convert startvalidtime and endvalidtime columns to datetime objects
    df['startvalidtime'] = pd.to_datetime(df['startvalidtime'])
    df['endvalidtime'] = pd.to_datetime(df['endvalidtime'])

    print(df.columns)
    def extract_id_column(df):
        # Iterate over each column in the DataFrame
        for col in df.columns:
            print(col)
            # Check if the column name contains the pattern ':ID('
            if ':ID(' in col:
                return col
        return None
    # Find the column that starts with 'instanceid:'
    id_column = extract_id_column(df)

    # List to hold rows for snapshot data
    snapshot_rows = []

    # Dictionary to map old IDs to new IDs
    id_mapping = []
    new_id = 0

    # Iterate over each row in the DataFrame
    for _, row in df.iterrows():
        start_time = row['startvalidtime']
        end_time = row['endvalidtime']

        # Generate snapshot rows for each day the entry spans
        while start_time < end_time + pd.Timedelta(days=1):
            next_day = start_time + pd.Timedelta(days=1)
            #if next_day > end_time:
             #   next_day = end_time

            # Create a new row for the snapshot data
            snapshot_row = row.copy()
            snapshot_row['startvalidtime'] = start_time
            snapshot_row['endvalidtime'] = next_day

            # Assign a new ID to the node
            old_id = row[id_column]
            new_id += 1

            snapshot_row[id_column] = new_id
            #print(snapshot_row)
            #print(id_column)
            mapping_row = snapshot_row[[id_column, 'startvalidtime']]
            mapping_row['old_id'] = old_id

            # Append the snapshot row to the list
            snapshot_rows.append(snapshot_row)
            id_mapping.append(mapping_row)

            start_time = next_day

    base_filename = os.path.basename(csv_file)
    # Remove the file extension
    base_filename = os.path.splitext(base_filename)[0]
    # Construct the output file path
    snapshot_file = os.path.join(snapshot_folder, f"{base_filename}.csv")
    id_mapping_file = os.path.join(id_mapping_folder, f"{base_filename}.csv")
    # Create a DataFrame from the snapshzot rows
    snapshot_df = pd.DataFrame(snapshot_rows)

    # Apply the formatting function to 'startvalidtime' and 'endvalidtime' columns
    snapshot_df['startvalidtime'] = snapshot_df['startvalidtime'].dt.strftime('%Y-%m-%dT%H:%M:%S')
    snapshot_df['endvalidtime'] = snapshot_df['endvalidtime'].dt.strftime('%Y-%m-%dT%H:%M:%S')
    
    # Write the snapshot DataFrame to a CSV file
    snapshot_df.to_csv(snapshot_file, index=False)
    # Create a DataFrame for the ID mapping
    id_mapping_df = pd.DataFrame(id_mapping)
    id_mapping_df.rename(columns={id_column : 'new_id'}, inplace=True)
    id_mapping_df.rename(columns={'startvalidtime' : 'validtime'}, inplace=True)
    # Write the ID mapping DataFrame to a CSV file
    id_mapping_df.to_csv(id_mapping_file, index=False)
    
# fonction to reconnect splited entities
def transfer_relationships(csv_file, mapping_folder, output_folder):
    # Read the CSV file containing the relationships
    df = pd.read_csv(csv_file, index_col=None)

    start_id_column = [col for col in df.columns if col.startswith(':START_ID')][0]
    end_id_column = [col for col in df.columns if col.startswith(':END_ID')][0]

    start_entity_label = start_id_column.split('(')[1].split(')')[0]
    start_mapping_path = os.path.join(mapping_folder, f"{start_entity_label}.csv")
    end_entity_label = end_id_column.split('(')[1].split(')')[0]
    end_mapping_path = os.path.join(mapping_folder, f"{end_entity_label}.csv")

    start_mapping_df = pd.read_csv(start_mapping_path)
    end_mapping_df = pd.read_csv(end_mapping_path)
    df['date'] = df['startvalidtime'].apply(lambda x: pd.to_datetime(x).strftime('%Y-%m-%d'))
    df['date1'] = df['date'].copy()
    
    df = pd.merge(df, start_mapping_df, left_on=[start_id_column, 'date'], right_on=['old_id', 'validtime'], how='left')
    df.drop(columns=[start_id_column, 'date', 'old_id', 'validtime'],inplace=True)
    df.rename(columns={'new_id': start_id_column}, inplace=True)

    df = pd.merge(df, end_mapping_df, left_on=[end_id_column, 'date1'], right_on=['old_id', 'validtime'], how='left')
    df.drop(columns=[end_id_column, 'date1', 'old_id', 'validtime'],inplace=True)
    df.rename(columns={'new_id': end_id_column}, inplace=True)

    # Extract the base filename from the input CSV file path
    base_filename = os.path.basename(csv_file)
    # Remove the file extension
    base_filename = os.path.splitext(base_filename)[0]
    try:
        df[start_id_column] = df[start_id_column].astype(int)
        df[end_id_column] = df[end_id_column].astype(int)
    except:
        print(base_filename)
        print(df[df[[start_id_column, end_id_column]].isna().any(axis=1)])
    # Construct the output file path
    output_file = os.path.join(output_folder, f"{base_filename}.csv")

    # Write the transferred relationships to a new CSV file
    df.to_csv(output_file, index=False)


entity_path = '/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/TG_data/Entity/'
relationship_path = '/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/TG_data/Relationship/'
mapping_path = '/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/id_mapping/'
output_path = '/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/'

entity_files = os.listdir(entity_path)
# Iterate over each relationship CSV file
for file_name in entity_files:
    # Construct the full file path
    file_path = os.path.join(entity_path, file_name)
    # Check if the file is a CSV file
    if file_name.endswith('.csv'):
        # Call the transfer_relationships function with the file path
        split_entities(file_path, output_path, mapping_path)
        
relationship_files = os.listdir(relationship_path)
# Iterate over each relationship CSV file
for file_name in relationship_files:
    # Construct the full file path
    file_path = os.path.join(relationship_path, file_name)
    # Check if the file is a CSV file
    if file_name.endswith('.csv'):
        # Call the transfer_relationships function with the file path
        transfer_relationships(file_path, mapping_path, output_path)