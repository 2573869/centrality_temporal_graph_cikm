import os
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

# Specify the csv file stock directory
csv_dir = "/users/sig/song/Exp_centrality/SE/D/csv/"
# Specify the plot file stock directory
plot_dir = "/users/sig/song/Exp_centrality/temporality/SE/"


# fonction to read and concat csv files in sepecified directory
def read_and_concat_csv(folder_path):
    # Get a list of all CSV files in the specified folder
    csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv')]

    # Check if there are any CSV files in the folder
    if not csv_files:
        print(f"No CSV files found in the folder: {folder_path}")
        return None

    # Initialize an empty DataFrame to store the concatenated data
    concatenated_df = pd.DataFrame()

    # Loop through each CSV file and concatenate its contents to the DataFrame
    for csv_file in csv_files:
        file_path = os.path.join(folder_path, csv_file)
        df = pd.read_csv(file_path)
        df.set_index(['EntityId'], inplace=True)
        concatenated_df = pd.concat([concatenated_df, df], axis=1)

    return concatenated_df

# fonction to product the plot demonstrating temporal centrality evolution in crossed perspectove
def centrality_evolution_crossed_perspective(df, entity_id, start_time, end_time):
    # Convert start_time and end_time to datetime without specifying a timezone
    start_time = pd.to_datetime(start_time)
    end_time = pd.to_datetime(end_time)

    # Filter the DataFrame for the specified time interval
    df_filtered = df.loc[:, start_time:end_time]
    df_filtered.columns = df_filtered.columns.strftime('%Y-%m-%d')
    # Plot the temporal centrality distribution for all entities (box plot)
    plt.figure(figsize=(15, 7))
    # Create the box plot
    sns.boxplot(data=df_filtered, boxprops=dict(facecolor='lightblue'),# Match default pandas color
    flierprops=dict(markerfacecolor='white', marker='o', markersize=6, linestyle='none'),  # Customize outliers
    medianprops=dict(color='green', linewidth=2))

    # Overlay the line plot for the specified entity
    df_entity = df_filtered.loc[entity_id, :]
    plt.plot(df_filtered.columns, df_entity.values, marker='o', color='b', linewidth=2)

    plt.xticks(rotation=45)
    plt.xlabel('Date')
    plt.ylabel('Temporal Degree Centrality')
    plt.tight_layout()
    plot_path = os.path.join(plot_dir,f'TDC_DistributionEvolution_Entity{entity_id}.pdf')
    plt.savefig(plot_path, bbox_inches='tight', format='pdf')
    plt.close()
    plt.show()

def individual_centrality_evolution(entity_ids, xtick_step=10):
    df = read_and_concat_csv(csv_dir)  # Assuming this function reads your data correctly
    df.fillna(0,inplace=True)
    # Filter the DataFrame to include only the given entity IDs
    filtered_df = df.loc[entity_ids]

    # Create subplots (1 row, 3 columns)
    fig, axs = plt.subplots(2, 3, figsize=(15, 6))  # Adjust figsize as needed

    # Flatten axs to easily iterate, as it is a 1D array (1 row, 3 columns)
    axs = axs.flatten()

    # Create a new x-tick list from 1, 2, 3, ..., len(df.columns)
    new_xticks = range(1, len(df.columns) + 1)

    # Plot the centrality of each entity in its own subplot
    for i, entity_id in enumerate(entity_ids):
        axs[i].plot(new_xticks, filtered_df.loc[entity_id], label=f'Entity {entity_id}')
        axs[i].set_title(f'Entity {entity_id}')
        axs[i].set_xlabel('Day')
        axs[i].set_ylabel('Temporal Degree Centrality')

        # Set x-ticks to 1, 2, 3, ..., len(df.columns) and apply xtick_step
        axs[i].set_xticks(new_xticks[::xtick_step])

        # Optionally, add a grid to make the plot clearer
        axs[i].grid(True)

    # If there are fewer subplots than spaces, hide the remaining empty subplots
    for j in range(len(entity_ids), len(axs)):
        axs[j].axis('off')  # Hide unused subplots

    plt.tight_layout()  # Adjust layout to prevent overlap

    # Save the plot as a PDF
    plt_path = os.path.join(plot_dir, 'centrality_evolution.pdf')
    plt.savefig(plt_path, bbox_inches='tight', format='pdf')

    # Show the plot
    plt.show()

# Example usage
df = read_and_concat_csv(csv_dir)
df.columns = pd.to_datetime(df.columns)
df = df.sort_index(axis=1)
df.fillna(0, inplace = True)
centrality_evolution_crossed_perspective(df, entity_id=4, start_time='2009-01-16', end_time='2009-01-30')
individual_centrality_evolution([67,33,62,15,26,60])
