import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os
import seaborn as sns

# Specify the csv file stock directory
csv_dir = "/users/sig/song/Exp_centrality/result_usability/corr/SE/csv/"
# Specify the plot file stock directory
plot_dir = "/users/sig/song/Exp_centrality/result_usability/corr/SE/"

""" 
    To indicate the semantics bias of temporal centrality metrics without semantical distinguishment,
    we calculated temporal degree centrality with all relationships and distincally for each type of 
    relationship on Social Experiment dataset, calculated their Pearson's ranking correlation coefficient 
    and finally producted a heatmap
"""

#Correlation coefficient calculation for semantically differently calculated temporal degree centrality 
def correlation_relationship(Relationship_list):
    data_degree_list = []
    
    # Read the 'All_relationships' data, calculate the mean, and rank
    df = pd.read_csv('/users/sig/song/Exp_centrality/result_usability/corr2/SE/csv/degree_all.csv', index_col='EntityId')
    df = df.mean(axis=1).rank(ascending=False)  # Replace values by rank
    df.name = 'All_relationships'
    data_degree_list.append(df)
    
    # Loop through Relationship_list, read files, calculate mean, and replace values by rank
    for r in Relationship_list:
        df = pd.read_csv(f'/users/sig/song/Exp_centrality/result_usability/corr2/SE/csv/degree_{r}.csv', 
                         index_col='EntityId').mean(axis=1).rank(ascending=False)
        df.name = r
        data_degree_list.append(df)
    
    # Concatenate all ranked DataFrames
    degree_df = pd.concat(data_degree_list, axis=1)
    for col in degree_df.columns:
        degree_df[col].fillna(degree_df[col].max()+1, inplace=True)  # Fill missing values with rank len(df) + 1
    
    # Calculate correlation matrix
    correlation_matrix = degree_df.corr(method='spearman')
    #correlation_matrix = degree_df.corr(method='kendall')
    
    # Display the correlation matrix as a heatmap
    plt.figure(figsize=(10, 8))
    sns.heatmap(correlation_matrix, cmap='coolwarm', annot=True, fmt=".2f", vmin=-1, vmax=1)
    #plt.title('Correlation Matrix of Relationship Rankings')
    plt_path = os.path.join(plot_dir, 'ranking_correlation_matrix_spearman_heatmap.pdf')
    plt.savefig(plt_path, bbox_inches='tight', format='pdf')
    plt.close()

    # Return the correlation matrix as a result if needed
    return correlation_matrix

def similarity(Relationship_list):
    data_degree_list = []
    
    # Read the 'All_relationships' data, calculate the mean, and rank
    df = pd.read_csv('/users/sig/song/Exp_centrality/result_usability/corr/SE/csv/degree_all.csv', index_col='EntityId')
    df = df.mean(axis=1).rank(ascending=False)  # Replace values by rank
    df.name = 'All_relationships'
    data_degree_list.append(df)
    
    # Loop through Relationship_list, read files, calculate mean, and replace values by rank
    for r in Relationship_list:
        df = pd.read_csv(f'/users/sig/song/Exp_centrality/result_usability/corr2/SE/csv/degree_{r}.csv', 
                         index_col='EntityId').mean(axis=1).rank(ascending=False)
        print(df)
        df.fillna(len(df)+1, inplace=True)  # Fill missing values with rank len(df) + 1
        print(df)
        df.name = r
        data_degree_list.append(df)
    
    # Concatenate all ranked DataFrames
    degree_df = pd.concat(data_degree_list, axis=1)
    
    # Step 1: Sort the DataFrame by the 'All_relationships' column in ascending order
    degree_df.sort_values(by='All_relationships', ascending=True, inplace=True)
    
    # Step 2: Replace values in other columns with squared distances
    for col in degree_df.columns:
        if col != 'All_relationships':
            degree_df[col].fillna(degree_df[col].max()+1, inplace=True)  # Fill missing values with rank len(df) + 1
            degree_df[col] = 1 - (degree_df[col] - degree_df['All_relationships']).abs()/len(degree_df)
    
    # Step 3: Drop the 'All_relationships' column
    degree_df.drop(columns=['All_relationships'], inplace=True)
    
    # Step 4: Transpose the DataFrame
    degree_df_transposed = degree_df.T
    
    # Step 5: Generate a heatmap for the transposed DataFrame
    plt.figure(figsize=(10, 2))
    ax = sns.heatmap(degree_df_transposed, cmap='Greys', annot=False, vmin=0, vmax=1)
    ax.set_xlabel('')
    ax.set_xticklabels([])
    ax.tick_params(axis='x', which='both', bottom=False, top=False)
    #plt.title('Heatmap of Squared Distance between Rankings')
    plt_path = os.path.join(plot_dir, 'similarity.pdf')
    plt.savefig(plt_path, bbox_inches='tight', format='pdf')
    plt.close()

# List for relationship labels
r_l = ['Call', 'ReceiveSMS', 'SendSMS', 'Proximity', 'BlogLivejournalTwitter', 'CloseFriend', 'FacebookAllTaggedPhotos', 'PoliticalDiscussant', 'SocializeTwicePerWeek']
# Calculation and heatmap production
correlation_relationship('Student')
similarity(r_l)