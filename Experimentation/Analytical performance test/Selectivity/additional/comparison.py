import pandas as pd
from scipy.stats import kendalltau
import os

def KendallTau(csv_path1, csv_path2):

    df1 = pd.read_csv(csv_path1)
    df2 = pd.read_csv(csv_path2)
    
    tau, p_value = kendalltau(df1['EntityId'], df2['EntityId'])
    
    return tau, p_value

# specify path to csv file with centrality ranking that you would like to compare
csv_dir1 = 'path1'
csv_dir2 = 'path2'

tau, p_value = KendallTau(csv_dir1, csv_dir2)
print(f"Kendall Tau correlation coefficient: {tau}")
print(f"P-value: {p_value}")

