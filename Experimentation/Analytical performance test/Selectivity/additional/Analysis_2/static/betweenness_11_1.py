import neo4j
import pandas as pd
import time
import matplotlib.pyplot as plt
import datetime
import os
import ast
from pandas import Timestamp
from neo4j import GraphDatabase, RoutingControl
from neo4j.exceptions import DriverError, Neo4jError

'''
For this test, we mainly 'borrowed' the code for temporal centrality with minor modification. 
By varying the length of a time unit (from 1 day to 10 days), the algorithm works equivalently 
to a static metric
'''

neo4j_url = "bolt://localhost:7687"
AUTH = ('neo4j', '0000')
neo4j_dir = "/projects/sig/song/neo4j_1/bin"
csv_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_with_meta-pass/static/csv/"
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.user_id'
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset

def conn():
    return GraphDatabase.driver(neo4j_url, auth=AUTH)

def MatchEntityID(driver,EntityLabel):
    # query to match entity id and state id
    query_entity_node = f"MATCH (n: {EntityLabel})" + f" RETURN {EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId"
    return driver.execute_query(query_entity_node,result_transformer_ = neo4j.Result.to_df)

time_axis=pd.DatetimeIndex([Timestamp('2016-02-26 00:00:00')])

def BetweennessInUnit(driver, EntityLabel, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):

    query_shortest_paths=f'''MATCH (n:{EntityLabel}) WHERE datetime(n.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(n.startvalidtime) < datetime('{UnitEndTime}') WITH collect(n) as Entities, collect(id(n)) as EntityIds CALL apoc.cypher.parallel2("MATCH path=AllShortestPaths((n:{EntityLabel}){"-" + f"[r:{RelationshipTypeStr}*]" if RelationshipTypeStr else "-[r*]"}-(m:{EntityLabel})) WITH *, relationships(path) AS rels, nodes(path) as nodes WHERE id(m) IN $EntityIds AND id(n)<id(m) AND ALL (rel IN rels WHERE datetime(rel.endvalidtime) >= datetime('{UnitStartTime}')) AND ALL (rel IN rels WHERE datetime(rel.startvalidtime) < datetime('{UnitEndTime}')) UNWIND nodes as node RETURN id(n) as start, id(m) as end, Collect(id(node)) as paths ", {{n: Entities, EntityIds: EntityIds}}, 'n') YIELD value RETURN value.start as start, value.end as end, value.paths as paths'''
    print(query_shortest_paths)
    df_paths=driver.execute_query(query_shortest_paths,result_transformer_ = neo4j.Result.to_df)
    df=MatchEntityID(driver,EntityLabel)
    print(df_paths)
    try :
        df_paths.loc[:,'nb_paths']=df_paths.apply(lambda x: x['paths'].count(x['start']), axis=1)
        df_paths['paths'] = df_paths.apply(lambda x: [value for value in x['paths'] if value not in [x['start'], x['end']]], axis=1)
        df['Betweenness'] = df.apply(lambda m: df_paths.apply(lambda n: n['paths'].count(m['NodeId']/n['nb_paths']), axis=1).sum(), axis=1)
    except :
        df['Betweenness'] = 0
    df = df.drop('NodeId', axis=1)
    return df

def BetweennessOnTimeline(driver, EntityLabel, RelationshipType=None):

    Betweenness=[]
    # if there is no restriction on relationship type
    if RelationshipType is None:
        # calculate degree centrality for every entity in every unit
        for i in range(0,len(time_axis)-1):
            t = datetime.datetime.isoformat(time_axis[i])
            # by varying the end time of the time unit, make the algorithm equivalent to static metric
            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=10))
            result_betweenness = BetweennessInUnit(driver,EntityLabel, t, t_after)
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_betweenness.set_index('EntityId',inplace=True)
                result_betweenness.columns=[str(t)]
            except:
                continue
            # collect result
            Betweenness.append(result_betweenness)
    # if there is a selection on relationship type
    else:
        # unify the order of relationship names
        RelationshipType.sort()
        # transform them to string with the separator for Neo4j CQL
        RelationshipTypeStr='|'.join(RelationshipType)

        for i in range(0,len(time_axis)-1):
            # calculate degree centrality for every entity in every unit
            t = datetime.datetime.isoformat(time_axis[i])
            t_after = t+pd.Timedelta(days=1)
            result_betweenness = BetweennessInUnit(driver,EntityLabel, t, t_after, RelationshipTypeStr)
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                result_betweenness.set_index('EntityId',inplace=True)
                result_betweenness.columns=[str(t)]
            except:
                continue
            Betweenness.append(result_betweenness)
    Betweenness_df=pd.concat(Betweenness, axis=1)
    Betweenness_df.fillna(0,inplace=True)
    return Betweenness_df

def BetweennessCentrality_result_to_csv(EntityLabel, RelationshipType=None):
    # connect to database
    driver=conn()
    # calculate the betweenness centrality
    df=BetweennessOnTimeline(driver, EntityLabel, RelationshipType)
    df.to_csv(csv_dir + '/betweenness11_1.csv')

def timing(EntityLabel, RelationshipType=None):
    time_start = time.time()
    BetweennessCentrality_result_to_csv(EntityLabel, RelationshipType=RelationshipType)
    time_end = time.time()
    time_c = time_end - time_start
    print('time cost', time_c, 's')
    return time_c

def import_social_experiment():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
      '--nodes=/users/sig/song/neo4j/import/Student.csv ' \
      '--nodes=/users/sig/song/neo4j/import/WLAN.csv ' \
      '--nodes=/users/sig/song/neo4j/import/State.csv ' \
      '--relationships=/users/sig/song/neo4j/import/Access.csv ' \
      '--relationships=/users/sig/song/neo4j/import/Call.csv ' \
      '--relationships=/users/sig/song/neo4j/import/MakeUp.csv ' \
      '--relationships=/users/sig/song/neo4j/import/ReceiveSMS.csv ' \
      '--relationships=/users/sig/song/neo4j/import/SendSMS.csv ' \
      '--relationships=/users/sig/song/neo4j/import/Proximity.csv ' \
      '--relationships=/users/sig/song/neo4j/import/BlogLivejournalTwitter.csv ' \
      '--relationships=/users/sig/song/neo4j/import/CloseFriend.csv ' \
      '--relationships=/users/sig/song/neo4j/import/FacebookAllTaggedPhotos.csv ' \
      '--relationships=/users/sig/song/neo4j/import/PoliticalDiscussant.csv ' \
      '--relationships=/users/sig/song/neo4j/import/SocializeTwicePerWeek.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes')

def import_snapshot_social_experiment():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Student.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Call.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/ReceiveSMS.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SendSMS.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Proximity.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/BlogLivejournalTwitter.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/CloseFriend.csv ' \
      '--relationships=//users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/FacebookAllTaggedPhotos.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/PoliticalDiscussant.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SocializeTwicePerWeek.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes')

def import_mathoverflow():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
      '--nodes=/projects/sig/song/neo4j/import/mathoverflow/user.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/answer.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/comments_to_answers.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/comments_to_questions.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes')


# Specify the Neo4j installation directory
os.chdir(neo4j_dir)

os.system('./neo4j start')

time.sleep(60)

timing(EntityLabel='User')

os.system('./neo4j stop')

