#!/bin/bash

python=/home/song/new_venv/bin/python

script=create_scripts.py 

echo "11" | ${python} ${script}

shell_scripts=$(find /home/song/Exp_centrality/qualitatif_comparison/metric_with_meta-pass/script -name "*.sh")

job_ids=""

echo "Submitting distributed sub-graph jobs..."
for script in $shell_scripts; do
    job_id=$(sbatch $script | awk '{print $4}')
    echo "Submitted job $job_id"
    job_ids+=":$job_id"
done

echo "Submitting aggregation job..."
aggregation_job_id=$(sbatch --dependency=afterok${job_ids} aggregation.sh | awk '{print $4}')
echo "Submitted job $aggregation_job_id with dependency on $job_ids"

echo "All jobs submitted."

