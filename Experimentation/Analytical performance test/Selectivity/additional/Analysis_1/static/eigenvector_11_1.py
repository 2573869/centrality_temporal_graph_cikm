import neo4j
import pandas as pd
import time
import matplotlib.pyplot as plt
import datetime
import os
import numpy as np
from scipy.linalg import eig
from pandas import Timestamp
from neo4j import GraphDatabase, RoutingControl
from neo4j.exceptions import DriverError, Neo4jError

'''
For this test, we mainly 'borrowed' the code for temporal centrality with minor modification. 
By varying the length of a time unit (from 1 day to the timeline length of the whole Social 
Experiment dataset), the algorithm works equivalently to a static metric
'''

neo4j_url = "bolt://localhost:7687"
AUTH = ('neo4j', '0000')
# in advance, you have to manually change the active database with respect to the volume you want to query
# in neo4j.conf set dbms.memory.max_heap_size = 4G
# Specify the Neo4j installation directory
neo4j_dir = "/projects/sig/song/neo4j_1/bin"
csv_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/static/csv"
# dictionary for dataframes to reduce the time of calculation
EntityIdAttributeNameDB='n.user_id'
# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset

def conn():
    return GraphDatabase.driver(neo4j_url, auth=AUTH)

time_axis=pd.DatetimeIndex([Timestamp('2009-01-09 00:00:00')])
print(time_axis)

def MatchEntityId(driver,EntityLabel):
    # query to match entity id and state id
    query_entity_node = f"MATCH (n: {EntityLabel})" + f" RETURN {EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId"
    return driver.execute_query(query_entity_node,result_transformer_ = neo4j.Result.to_df)

def EigenvectorInUnit(driver, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):
    # query for degree in one unit, maybe with relationships chosen
    query_adjacency = f"""MATCH (n){'-' + f'[r:{RelationshipTypeStr}]' if RelationshipTypeStr else '-[r]'}-(m) WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN id(n) AS NodeId, COLLECT(DISTINCT(id(m))) AS AdjacentNodes"""
    print(query_adjacency)
    df = driver.execute_query(query_adjacency, result_transformer_ = neo4j.Result.to_df)
    print(df)
    adj_matrix = np.zeros((len(df), len(df)))
    print('MC')
    # Function to map linked node IDs to their indices using the DataFrame's inherent index

    def map_indices(row, df):
        return [df.index[df['NodeId'] == node].tolist()[0] for node in row['AdjacentNodes']]
    # Apply the function to create a new column
    df['entity_mapping'] = df.apply(map_indices, axis=1, args=(df,))
    print(df)
    for idx, row in df.iterrows():
        for node in row['entity_mapping']:
            adj_matrix[idx - 1][node - 1] += 1
    print(adj_matrix)
    # Calculate eigenvectors and eigenvalues
    eigenvalues, eigenvectors = eig(adj_matrix)
    # Find the index of the eigenvalue with the largest magnitude
    max_index = np.argmax(np.abs(eigenvalues))
    print(max_index)
    # Get the corresponding eigenvector
    max_eigenvector = eigenvectors[:, max_index]
    # Normalize the eigenvector
    eigenvector_centrality = max_eigenvector / np.sum(max_eigenvector)
    print(eigenvector_centrality)
    # Add eigenvector centrality values as a new column to the dataframe
    df['Eigenvector'] = eigenvector_centrality.real
    print(df)
    return df.loc[:,['NodeId','Eigenvector']].copy()

def EigenvectorOnTimeline(driver, EntityLabel, RelationshipType=None):
    NodeEigenvector = []
    # if there is no restriction on relationship type
    if RelationshipType is None:
        # calculate degree centrality for every entity in every unit
        for i in range(0, len(time_axis)):
            t = datetime.datetime.isoformat(time_axis[i])
            # by varying the end time of the time unit, make the algorithm equivalent to static metric 
            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=106))
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                df = EigenvectorInUnit(driver, t, t_after)
                df.set_index('NodeId', inplace=True)
                df.columns = [str(t)]
            except:
                continue
            # collect result
            NodeEigenvector.append(df)
    # if there is a selection on relationship type
    else:
        # unify the order of relationship names
        RelationshipType.sort()
        # transform them to string with the separator for Neo4j CQL
        RelationshipTypeStr = '|'.join(RelationshipType)
        for i in range(0, len(time_axis)):
            # calculate degree centrality for every entity in every unit
            t = datetime.datetime.isoformat(time_axis[i])
            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))
            # to avoid error when in one time unit there is no relationship and the step above returns nothing
            try:
                df = EigenvectorInUnit(driver, t, t_after)
                df.set_index('NodeId', inplace=True)
                df.columns = [str(t)]
            except:
                continue
            NodeEigenvector.append(df)
    NodeEigenvector_df = pd.concat(NodeEigenvector, axis=1)
    NodeEigenvector_df.fillna(0, inplace=True)
    print(NodeEigenvector_df)
    return NodeEigenvector_df

def EigenvectorCentrality_to_csv(EntityLabel, RelationshipType=None):
    # connect to database
    driver=conn()
    # calculate the betweenness centrality
    df_entity_id=MatchEntityId(driver,EntityLabel)
    print(df_entity_id)
    df_eigenvector = EigenvectorOnTimeline(driver, EntityLabel, RelationshipType)
    df_eigenvector_reset = df_eigenvector.reset_index()
    df = pd.merge(df_entity_id, df_eigenvector_reset, on='NodeId', how='inner')
    df.drop('NodeId', axis=1, inplace=True)
    df = df.groupby(['EntityId']).sum()
    df.to_csv(csv_dir + '/eigenvector11_1.csv')

def timing(EntityLabel,RelationshipType=None):
    time_start = time.time()
    EigenvectorCentrality_to_csv(EntityLabel,RelationshipType=RelationshipType)
    time_end = time.time()
    time_c = time_end - time_start
    print('time cost', time_c, 's')

def import_social_experiment():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
      '--nodes=/projects/sig/song/neo4j/import/Student.csv ' \
      #'--nodes=/projects/sig/song/neo4j/import/WLAN.csv ' \
      '--nodes=/projects/sig/song/neo4j/import/State.csv ' \
      #'--relationships=/projects/sig/song/neo4j/import/Access.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/Call.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/MakeUp.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/ReceiveSMS.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/SendSMS.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/Proximity.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/BlogLivejournalTwitter.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/CloseFriend.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/FacebookAllTaggedPhotos.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/PoliticalDiscussant.csv ' \
      '--relationships=/projects/sig/song/neo4j/import/SocializeTwicePerWeek.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes')

def import_snapshot_social_experiment():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Student.csv ' \
      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/WLAN.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Access.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Call.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/ReceiveSMS.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SendSMS.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Proximity.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/BlogLivejournalTwitter.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/CloseFriend.csv ' \
      '--relationships=//users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/FacebookAllTaggedPhotos.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/PoliticalDiscussant.csv ' \
      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SocializeTwicePerWeek.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes')

def import_e_commerce():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j --nodes=/users/sig/song/neo4j/import/user.csv --nodes=/users/sig/song/neo4j/import/category.csv --nodes=/users/sig/song/neo4j/import/item1_5.csv --nodes=/users/sig/song/neo4j/import/item6_10.csv --nodes=/users/sig/song/neo4j/import/item11_15.csv --nodes=/users/sig/song/neo4j/import/item16_20.csv --nodes=/users/sig/song/neo4j/import/item26_30.csv --nodes=/users/sig/song/neo4j/import/item31_34.csv --relationships=/users/sig/song/neo4j/import/addtocart.csv --relationships=/users/sig/song/neo4j/import/transaction.csv --relationships=/users/sig/song/neo4j/import/view.csv --relationships=/users/sig/song/neo4j/import/belongto.csv --relationships=/users/sig/song/neo4j/import/subCategory.csv --skip-bad-relationships --skip-duplicate-nodes')

def import_city_bike():
    # import data
    os.system('./neo4j-admin import --force --database=neo4j ' \
              '--nodes=/users/sig/song/neo4j/import/station.csv ' \
              '--relationships=/users/sig/song/neo4j/import/202102_trip.csv ' \
              #'--relationships=/users/sig/song/neo4j/import/2020_trip.csv ' \
              #'--relationships=/users/sig/song/neo4j/import/202101_trip.csv ' \
              '--relationships=/users/sig/song/neo4j/import/202103_trip.csv ' \
              #'--relationships=/users/sig/song/neo4j/import/202104_trip.csv ' \
              #'--relationships=/users/sig/song/neo4j/import/202105_trip.csv ' \
              '--skip-bad-relationships --skip-duplicate-nodes')

# Change the working directory to the Neo4j installation directory
os.chdir(neo4j_dir)

import_social_experiment()
#import_city_bike()
# Start Neo4j
os.system('./neo4j start')

time.sleep(60)

timing(EntityLabel='Student')

os.system('./neo4j stop')
