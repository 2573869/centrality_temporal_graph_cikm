import pandas as pd
import math
import os
import sys
import numpy as np

def clear_folder(folder_path):
    # Iterate over all files and subdirectories in the folder
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
            # Remove each file
            file_path = os.path.join(root, name)
            os.remove(file_path)
        for name in dirs:
            # Remove each subdirectory
            dir_path = os.path.join(root, name)
            os.rmdir(dir_path)


script_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/selective/script"
csv_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/selective/csv"
EntityIdAttributeNameDB = "n.user_id"

def generate_py(nb_machine, IntervalStartUnit, IntervalEndUnit, TimeUnit):
    start_interval_datetime = pd.to_datetime(IntervalStartUnit)
    end_interval_datetime = pd.to_datetime(IntervalEndUnit)
    timeline = pd.date_range(start_interval_datetime, end_interval_datetime, freq=TimeUnit)
    timeline_list = timeline.tolist()
    np.random.shuffle(timeline_list)
    np.random.shuffle(timeline_list)
    np.random.shuffle(timeline_list)
    for i in range(nb_machine):
        sub_tl = timeline_list[i * math.ceil(len(timeline) / nb_machine):min((i + 1) * math.ceil(len(timeline) / nb_machine), len(timeline))+1]
        sub_tl_str = sub_tl.__str__()

        generated_code = ("import neo4j\n"
                          "import pandas as pd\n"
                          "import time\n"
                          "import matplotlib.pyplot as plt\n"
                          "import datetime\n"
                          "import os\n"
                          "import numpy as np\n"
                          "from scipy.linalg import eig\n"
                          "from pandas import Timestamp\n"
                          "from neo4j import GraphDatabase, RoutingControl\n"
                          "from neo4j.exceptions import DriverError, Neo4jError\n"
                          "\n"
                          "neo4j_url = \"bolt://localhost:7687\"\n"
                          "AUTH = ('neo4j', '0000')\n"
                          "# in advance, you have to manually change the active database with respect to the volume you want to query\n"
                          "# in neo4j.conf set dbms.memory.max_heap_size = 4G\n"
                          "# Specify the Neo4j installation directory\n"
                          f"neo4j_dir = \"/projects/sig/song/neo4j_{str(i + 1)}/bin\"\n"
                          f"csv_dir = \"{csv_dir}\"\n"
                          "# dictionary for dataframes to reduce the time of calculation\n"
                          f"EntityIdAttributeNameDB='{EntityIdAttributeNameDB}'\n"
                          "# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset\n"
                          "\n"
                          "def conn():\n"
                          "    return GraphDatabase.driver(neo4j_url, auth=AUTH)\n"
                          "\n"
                          f"time_axis=pd.DatetimeIndex({sub_tl_str})\n"
                          "print(time_axis)\n"
                          "\n"
                          "def MatchEntityId(driver,EntityLabel):\n"
                          "    # query to match entity id and state id\n"
                          "    query_entity_node = f\"MATCH (n: {EntityLabel})\" + f\" RETURN {EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId\"\n"
                          "    return driver.execute_query(query_entity_node,result_transformer_ = neo4j.Result.to_df)\n"
                          "\n"
                          "def EigenvectorInUnit(driver, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):\n"
                          "    # query for degree in one unit, maybe with relationships chosen\n"
                          "    query_adjacency = f\"\"\"MATCH (n){'-' + f'[r:{RelationshipTypeStr}]' if RelationshipTypeStr else '-[r]'}-(m) WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN id(n) AS NodeId, COLLECT(DISTINCT(id(m))) AS AdjacentNodes\"\"\"\n"
                          "    print(query_adjacency)\n"
                          "    df = driver.execute_query(query_adjacency, result_transformer_ = neo4j.Result.to_df)\n"
                          "    print(df)\n"
                          "    adj_matrix = np.zeros((len(df), len(df)))\n"
                          "    print('MC')\n"
                          "    # Function to map linked node IDs to their indices using the DataFrame's inherent index\n"
                          "\n"
                          "    def map_indices(row, df):\n"
                          "        return [df.index[df['NodeId'] == node].tolist()[0] for node in row['AdjacentNodes']]\n"
                          "    # Apply the function to create a new column\n"
                          "    df['entity_mapping'] = df.apply(map_indices, axis=1, args=(df,))\n"
                          "    print(df)\n"
                          "    for idx, row in df.iterrows():\n"
                          "        for node in row['entity_mapping']:\n"
                          "            adj_matrix[idx - 1][node - 1] += 1\n"
                          "    print(adj_matrix)\n"
                          "    # Calculate eigenvectors and eigenvalues\n"
                          "    eigenvalues, eigenvectors = eig(adj_matrix)\n"
                          "    # Find the index of the eigenvalue with the largest magnitude\n"
                          "    max_index = np.argmax(np.abs(eigenvalues))\n"
                          "    print(max_index)\n"
                          "    # Get the corresponding eigenvector\n"
                          "    max_eigenvector = eigenvectors[:, max_index]\n"
                          "    # Normalize the eigenvector\n"
                          "    eigenvector_centrality = max_eigenvector / np.sum(max_eigenvector)\n"
                          "    print(eigenvector_centrality)\n"
                          "    # Add eigenvector centrality values as a new column to the dataframe\n"
                          "    df['Eigenvector'] = eigenvector_centrality.real\n"
                          "    print(df)\n"
                          "    return df.loc[:,['NodeId','Eigenvector']].copy()\n"
                          "\n"
                          "def EigenvectorOnTimeline(driver, EntityLabel, RelationshipType=None):\n"
                          "    NodeEigenvector = []\n"
                          "    # if there is no restriction on relationship type\n"
                          "    if RelationshipType is None:\n"
                          "        # calculate degree centrality for every entity in every unit\n"
                          "        for i in range(0, len(time_axis)):\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                df = EigenvectorInUnit(driver, t, t_after)\n"
                          "                df.set_index('NodeId', inplace=True)\n"
                          "                df.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            # collect result\n"
                          "            NodeEigenvector.append(df)\n"
                          "    # if there is a selection on relationship type\n"
                          "    else:\n"
                          "        # unify the order of relationship names\n"
                          "        RelationshipType.sort()\n"
                          "        # transform them to string with the separator for Neo4j CQL\n"
                          "        RelationshipTypeStr = '|'.join(RelationshipType)\n"
                          "        for i in range(0, len(time_axis)):\n"
                          "            # calculate degree centrality for every entity in every unit\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                df = EigenvectorInUnit(driver, t, t_after)\n"
                          "                df.set_index('NodeId', inplace=True)\n"
                          "                df.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            NodeEigenvector.append(df)\n"
                          "    NodeEigenvector_df = pd.concat(NodeEigenvector, axis=1)\n"
                          "    NodeEigenvector_df.fillna(0, inplace=True)\n"
                          "    print(NodeEigenvector_df)\n" 
                          "    return NodeEigenvector_df\n"
                          "\n"
                          "def EigenvectorCentrality_to_csv(EntityLabel, RelationshipType=None):\n"
                          "    # connect to database\n"
                          "    driver=conn()\n"
                          "    # calculate the betweenness centrality\n"
                          "    df_entity_id=MatchEntityId(driver,EntityLabel)\n"
                          "    print(df_entity_id)\n"
                          "    df_eigenvector = EigenvectorOnTimeline(driver, EntityLabel, RelationshipType)\n"
                          "    df_eigenvector_reset = df_eigenvector.reset_index()\n"
                          "    df = pd.merge(df_entity_id, df_eigenvector_reset, on='NodeId', how='inner')\n"
                          "    df.drop('NodeId', axis=1, inplace=True)\n"
                          "    df = df.groupby(['EntityId']).sum()\n"
                          f"    df.to_csv(csv_dir + '/eigenvector{nb_machine}_{i + 1}.csv')\n"
                          "\n"
                          "def timing(EntityLabel,RelationshipType=None):\n"
                          "    time_start = time.time()\n"
                          "    EigenvectorCentrality_to_csv(EntityLabel,RelationshipType=RelationshipType)\n"
                          "    time_end = time.time()\n"
                          "    time_c = time_end - time_start\n"
                          "    print('time cost', time_c, 's')\n"
                          "\n"
                          "def import_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/projects/sig/song/neo4j/import/Student.csv ' \\\n"
                          "      #'--nodes=/projects/sig/song/neo4j/import/WLAN.csv ' \\\n"
                          "      '--nodes=/projects/sig/song/neo4j/import/State.csv ' \\\n"
                          "      #'--relationships=/projects/sig/song/neo4j/import/Access.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/Call.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/MakeUp.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/SendSMS.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/Proximity.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/CloseFriend.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_snapshot_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Student.csv ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/WLAN.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Access.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Call.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SendSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Proximity.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/CloseFriend.csv ' \\\n"
                          "      '--relationships=//users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_e_commerce():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j --nodes=/users/sig/song/neo4j/import/user.csv --nodes=/users/sig/song/neo4j/import/category.csv --nodes=/users/sig/song/neo4j/import/item1_5.csv --nodes=/users/sig/song/neo4j/import/item6_10.csv --nodes=/users/sig/song/neo4j/import/item11_15.csv --nodes=/users/sig/song/neo4j/import/item16_20.csv --nodes=/users/sig/song/neo4j/import/item26_30.csv --nodes=/users/sig/song/neo4j/import/item31_34.csv --relationships=/users/sig/song/neo4j/import/addtocart.csv --relationships=/users/sig/song/neo4j/import/transaction.csv --relationships=/users/sig/song/neo4j/import/view.csv --relationships=/users/sig/song/neo4j/import/belongto.csv --relationships=/users/sig/song/neo4j/import/subCategory.csv --skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_city_bike():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "              '--nodes=/users/sig/song/neo4j/import/station.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202102_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/2020_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202101_trip.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202103_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202104_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202105_trip.csv ' \\\n"
                          "              '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "# Change the working directory to the Neo4j installation directory\n"
                          "os.chdir(neo4j_dir)\n"
                          "\n"
                          "import_social_experiment()\n"
                          "#import_city_bike()\n"
                          "# Start Neo4j\n"
                          "os.system('./neo4j start')\n"
                          "\n"
                          "time.sleep(60)\n"
                          "\n"
                          f"timing(EntityLabel='Student', RelationshipType=['BlogLivejournalTwitter','FacebookAllTaggedPhotos'])\n"
                          "\n"
                          "os.system('./neo4j stop')\n")

        os.chdir(script_dir)
        with open(f'eigenvector_{nb_machine}_{str(i + 1)}.py', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to eigenvector_{nb_machine}_{str(i + 1)}.py")


python = '/home/song/new_venv/bin/python'


def generate_sh(nb_machine, task_name, python, partition, nb_task, cpu_per_task):
    for i in range(nb_machine):
        generated_code = f"""#!/bin/bash

#SBATCH --job-name={nb_machine}_{str(i + 1)}{task_name}
#SBATCH --output={task_name}{nb_machine}_{str(i + 1)}.out
#SBATCH --error={task_name}{nb_machine}_{str(i + 1)}.er

#SBATCH --partition={partition}

#SBATCH --ntasks={nb_task}
#SBATCH --cpus-per-task={cpu_per_task} 

python={python}

script={script_dir}/{task_name}_{nb_machine}_{str(i + 1)}.py

srun ${{python}} ${{script}}
"""
        os.chdir(script_dir)
        with open(f'eigenvector_{nb_machine}_{str(i + 1)}.sh', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to eigenvector_{nb_machine}_{str(i + 1)}.sh")


nb_machine = int(sys.stdin.read())

clear_folder(script_dir)
#clear_folder(csv_dir)

generate_py(nb_machine, '2009-01-09T00:00:00', '2009-04-24T00:00:00', 'd')
generate_sh(nb_machine, 'eigenvector', python, '24CPUNodes', '1', '24')
