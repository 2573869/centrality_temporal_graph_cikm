#!/bin/bash

#SBATCH --job-name=aggregation
#SBATCH --output=aggregation.out
#SBATCH --error=aggregation.er

#SBATCH --partition=48CPUNodes

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8

python=/home/song/new_venv/bin/python

script=aggregation.py 

srun ${python} ${script}
