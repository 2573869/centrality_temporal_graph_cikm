import os
import pandas as pd
from matplotlib import pyplot as plt

csv_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/csv"
plot_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/plot"
score_dir = "/home/song/Exp_centrality/qualitatif_comparison/metric_without_meta-pass/score"

def read_and_concat_csv(folder_path):
    # Get a list of all CSV files in the specified folder
    csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv')]

    # Check if there are any CSV files in the folder
    if not csv_files:
        print(f"No CSV files found in the folder: {folder_path}")
        return None

    # Initialize an empty DataFrame to store the concatenated data
    concatenated_df = pd.DataFrame()

    # Loop through each CSV file and concatenate its contents to the DataFrame
    for csv_file in csv_files:
        file_path = os.path.join(folder_path, csv_file)
        df = pd.read_csv(file_path)
        df.set_index(['EntityId'], inplace=True)
        concatenated_df = pd.concat([concatenated_df, df],axis=1)
    
    return concatenated_df

def Position(EntitiesEigenvector, EntityIdentifier=None, show_distribution=True):
    """
    Sort the temporal degree centrality of all entities with the same label and show their distribution if required.

    Parameters:
        EntitiesEigenvector (pd.DataFrame): A DataFrame with the temporal eigenvector centrality of each entity with the given label.
        EntityIdentifier (str, optional): The identifier of a specific entity to analyze. If not specified, the distribution of degree centrality across all entities will be shown.
        show_distribution (bool, optional): Whether to show a plot of the degree distribution across entities.

    Returns:
        None
    """
    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show degree centrality of every entity with the label chosen and their distribution
        EntitiesEigenvector.columns=['Eigenvector']
        EntitiesEigenvector=EntitiesEigenvector.sort_values(by='Eigenvector',ascending=False)
        EntitiesEigenvector['Rank']=range(1,EntitiesEigenvector.shape[0]+1)
        if show_distribution :
            EntitiesEigenvector['Eigenvector'].plot(kind='box')
            plot_path = os.path.join(plot_dir,'EigenvectorDistribution.pdf')
            plt.savefig(plot_path, bbox_inches='tight', format='pdf')
            EntitiesEigenvector['Position']=EntitiesEigenvector['Rank']/EntitiesEigenvector.shape[0]
        score_path = os.path.join(score_dir,"score.csv")
        EntitiesEigenvector.to_csv(score_path)
        print(EntitiesEigenvector)
    # if it is not for a special time interval
    else:
        # show eigenvector centrality of chosen entity with its rank and position among all entities with the same label
        EntitiesEigenvector.columns=['Eigenvector']
        EntitiesEigenvector=EntitiesEigenvector.sort_values(by='Eigenvector',ascending=False)
        EntitiesEigenvector['Rank']=range(1,EntitiesEigenvector.shape[0]+1)
        if show_distribution :
            EntitiesEigenvector['Eigenvector'].plot(kind='box')
            plt.axhline(y=EntitiesEigenvector.loc[EntityIdentifier].iloc[0],ls='--',label='Entity')
            plt.legend()
            plot_path = os.path.join(plot_dir,'EigenvectorDistribution.pdf')
            plt.savefig(plot_path, bbox_inches='tight', format='pdf')
            plt.close() 
            EntitiesEigenvector['Position']=EntitiesEigenvector['Rank']/EntitiesEigenvector.shape[0]
        score_path = os.path.join(score_dir,"score.csv")
        EntitiesEigenvector.to_csv(score_path)
        print(EntitiesEigenvector.loc[EntityIdentifier])

def Evolution(EigenvectorInEveryUnit, EntityIdentifier):
    """
    Plots the eigenvector evolution of an entity over time.

    Parameters:
        DegreeInEveryUnit (pd.DataFrame): A DataFrame with the temporal degree of each entity.
        EntityIdentifier (str): The identifier of the entity for which to plot the degree evolution.

    Returns:
        None
    """
    # show degree evolution of an entity in its valid time
    ax = EigenvectorInEveryUnit.loc[EntityIdentifier].plot()
    # Set the rotation angle of x-axis labels
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    plot_path=os.path.join(plot_dir,f'EigenvectorEvolution_SE_{EntityIdentifier}.pdf')
    plt.savefig(plot_path, bbox_inches='tight', format='pdf')
    # Display the plot
    plt.close() 

def EigenvectorCentrality(csv_folder_path, EntityIdentifier=None,show_distribution=True):

    df=read_and_concat_csv(csv_folder_path)

    #print(df)
    #print(pd.DataFrame(df.mean(axis=1)))
    # if it is not specially for an entity
    if EntityIdentifier is None:
        # show the distribution curve and degree table
        Position(pd.DataFrame(df.mean(axis=1)),show_distribution=show_distribution)
    # if it is not for a special time interval
    else :
        # show the distribution curve and the position of the entity
        Position(pd.DataFrame(df.mean(axis=1)),EntityIdentifier,show_distribution=show_distribution)
        # show degree evolution of an entity in its valid time
        Evolution(df, EntityIdentifier)
        
def TopEntitiesEvolution(csv_folder_path, top_n=5):
    df = read_and_concat_csv(csv_folder_path)
    average_betweenness = df.mean(axis=1).sort_values(ascending=False)
    top_entities = average_betweenness.head(top_n).index

    for entity in top_entities:
        Evolution(df, entity)
        

TopEntitiesEvolution(csv_dir, top_n=5)
EigenvectorCentrality(csv_dir, show_distribution=True)
