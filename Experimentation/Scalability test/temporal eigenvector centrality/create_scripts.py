import pandas as pd
import math
import os
import sys
import numpy as np

def clear_folder(folder_path):
    # Iterate over all files and subdirectories in the folder
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
            # Remove each file
            file_path = os.path.join(root, name)
            os.remove(file_path)
        for name in dirs:
            # Remove each subdirectory
            dir_path = os.path.join(root, name)
            os.rmdir(dir_path)


script_dir = "/users/sig/song/Exp_centrality/Eigenvector/test/script"
csv_dir = "/users/sig/song/Exp_centrality/Eigenvector/test/csv"
EntityIdAttributeNameDB = "n.user_id"
TOTAL_NUM_RELS = "2168270"


def generate_py(nb_machine, IntervalStartUnit, IntervalEndUnit, TimeUnit):
    start_interval_datetime = pd.to_datetime(IntervalStartUnit)
    end_interval_datetime = pd.to_datetime(IntervalEndUnit)
    timeline = pd.date_range(start_interval_datetime, end_interval_datetime, freq=TimeUnit)
    timelines={}
    for m in range(50):
        timeline_list = timeline.tolist()
        np.random.shuffle(timeline_list)
        np.random.shuffle(timeline_list)
        np.random.shuffle(timeline_list)
        timelines[f"list{m}"]=timeline_list
    for i in range(nb_machine):
        sub_timelines={}
        for k in range(50):

            sub_timelines[f"sub_tl{k}"] = timelines[f"list{k}"][i * math.ceil(len(timeline) / nb_machine):min((i + 1) * math.ceil(len(timeline) / nb_machine), len(timeline))]

        sub_tls_str = sub_timelines.__str__()

        generated_code = ("import neo4j\n"
                          "import pandas as pd\n"
                          "import time\n"
                          "import matplotlib.pyplot as plt\n"
                          "import datetime\n"
                          "import os\n"
                          "import ast\n"
                          "import random\n"
                          "from pandas import Timestamp\n"
                          "from neo4j import GraphDatabase, RoutingControl\n"
                          "from neo4j.exceptions import DriverError, Neo4jError\n"
                          "\n"
                          "neo4j_url = \"bolt://localhost:7687\"\n"
                          "AUTH = ('neo4j', '0000')\n"
                          "# in advance, you have to manually change the active database with respect to the volume you want to query\n"
                          "# Specify the Neo4j installation directory\n"
                          f"neo4j_dir = \"/users/sig/song/neo4j_{str(i + 1)}/bin\"\n"
                          f"csv_dir = \"{csv_dir}\"\n"
                          f"time_axises=eval(\"{sub_tls_str}\")"
                          "# dictionary for dataframes to reduce the time of calculation\n"
                          f"EntityIdAttributeNameDB='{EntityIdAttributeNameDB}'\n"
                          f"TOTAL_NUM_RELS = {TOTAL_NUM_RELS}"
                          "# you have to change this according to how the attribute corresponding to entity identifier is called in your dataset\n"
                          "\n"
                          "def conn():\n"
                          "    return GraphDatabase.driver(neo4j_url, auth=AUTH)\n"
                          "\n"
                          "def MatchEntityId(driver,EntityLabel):\n"
                          "    # query to match entity id and state id\n"
                          "    query_entity_node = f\"MATCH (n: {EntityLabel})\" + f\" RETURN {EntityIdAttributeNameDB} AS EntityId, id(n) as NodeId\"\n"
                          "    return driver.execute_query(query_entity_node,result_transformer_ = neo4j.Result.to_df)\n"
                          "\n"
                          "def EigenvectorInUnit(driver, UnitStartTime, UnitEndTime, RelationshipTypeStr=None):\n"
                          "    # query for degree in one unit, maybe with relationships chosen\n"
                          "    query_adjacency = f\"\"\"MATCH (n){'-' + f'[r:{RelationshipTypeStr}]' if RelationshipTypeStr else '-[r]'}-(m) WHERE datetime(r.endvalidtime) >= datetime('{UnitStartTime}') AND datetime(r.startvalidtime) < datetime('{UnitEndTime}') RETURN id(n) AS NodeId, COLLECT(DISTINCT(id(m))) AS AdjacentNodes\"\"\"\n"
                          "    print(query_adjacency)\n"
                          "    df = driver.execute_query(query_adjacency, result_transformer_ = neo4j.Result.to_df)\n"
                          "    print(df)\n"
                          "    adj_matrix = np.zeros((len(df), len(df)))\n"
                          "    print('MC')\n"
                          "    # Function to map linked node IDs to their indices using the DataFrame's inherent index\n"
                          "\n"
                          "    def map_indices(row, df):\n"
                          "        return [df.index[df['NodeId'] == node].tolist()[0] for node in row['AdjacentNodes']]\n"
                          "    # Apply the function to create a new column\n"
                          "    df['entity_mapping'] = df.apply(map_indices, axis=1, args=(df,))\n"
                          "    print(df)\n"
                          "    for idx, row in df.iterrows():\n"
                          "        for node in row['entity_mapping']:\n"
                          "            adj_matrix[idx - 1][node - 1] += 1\n"
                          "    print(adj_matrix)\n"
                          "    # Calculate eigenvectors and eigenvalues\n"
                          "    eigenvalues, eigenvectors = eig(adj_matrix)\n"
                          "    # Find the index of the eigenvalue with the largest magnitude\n"
                          "    max_index = np.argmax(np.abs(eigenvalues))\n"
                          "    print(max_index)\n"
                          "    # Get the corresponding eigenvector\n"
                          "    max_eigenvector = eigenvectors[:, max_index]\n"
                          "    # Normalize the eigenvector\n"
                          "    eigenvector_centrality = max_eigenvector / np.sum(max_eigenvector)\n"
                          "    print(eigenvector_centrality)\n"
                          "    # Add eigenvector centrality values as a new column to the dataframe\n"
                          "    df['Eigenvector'] = eigenvector_centrality.real\n"
                          "    print(df)\n"
                          "    return df.loc[:,['NodeId','Eigenvector']].copy()\n"
                          "\n"
                          "def EigenvectorOnTimeline(driver, time_axis, EntityLabel, RelationshipType=None):\n"
                          "    NodeEigenvector = []\n"
                          "    # if there is no restriction on relationship type\n"
                          "    if RelationshipType is None:\n"
                          "        # calculate degree centrality for every entity in every unit\n"
                          "        for i in range(0, len(time_axis)):\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                df = EigenvectorInUnit(driver, t, t_after)\n"
                          "                df.set_index('NodeId', inplace=True)\n"
                          "                df.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            # collect result\n"
                          "            NodeEigenvector.append(df)\n"
                          "    # if there is a selection on relationship type\n"
                          "    else:\n"
                          "        # unify the order of relationship names\n"
                          "        RelationshipType.sort()\n"
                          "        # transform them to string with the separator for Neo4j CQL\n"
                          "        RelationshipTypeStr = '|'.join(RelationshipType)\n"
                          "        for i in range(0, len(time_axis)):\n"
                          "            # calculate degree centrality for every entity in every unit\n"
                          "            t = datetime.datetime.isoformat(time_axis[i])\n"
                          "            t_after = datetime.datetime.isoformat(time_axis[i]+pd.Timedelta(days=1))\n"
                          "            # to avoid error when in one time unit there is no relationship and the step above returns nothing\n"
                          "            try:\n"
                          "                df = EigenvectorInUnit(driver, t, t_after)\n"
                          "                df.set_index('NodeId', inplace=True)\n"
                          "                df.columns = [str(t)]\n"
                          "            except:\n"
                          "                continue\n"
                          "            NodeEigenvector.append(df)\n"
                          "    NodeEigenvector_df = pd.concat(NodeEigenvector, axis=1)\n"
                          "    NodeEigenvector_df.fillna(0, inplace=True)\n"
                          "    print(NodeEigenvector_df)\n" 
                          "    return NodeEigenvector_df\n"
                          "\n"
                          "def EigenvectorCentrality_to_csv(time_axis, EntityLabel, RelationshipType=None):\n"
                          "    # connect to database\n"
                          "    driver=conn()\n"
                          "    # calculate the betweenness centrality\n"
                          "    df_entity_id=MatchEntityId(driver,EntityLabel)\n"
                          "    print(df_entity_id)\n"
                          "    df_eigenvector = EigenvectorOnTimeline(driver, EntityLabel, RelationshipType)\n"
                          "    df_eigenvector_reset = df_eigenvector.reset_index()\n"
                          "    df = pd.merge(df_entity_id, df_eigenvector_reset, on='NodeId', how='inner')\n"
                          "    df.drop('NodeId', axis=1, inplace=True)\n"
                          "    df = df.groupby(['EntityId']).sum()\n"
                          f"    df.to_csv(csv_dir + '/eigenvector{nb_machine}_{i + 1}.csv')\n"
                          "\n"
                          "def timing(time_axis, EntityLabel, RelationshipType=None):\n"
                          "    time_start = time.time()\n"
                          "    EigenvectorCentrality_to_csv(time_axis, EntityLabel,RelationshipType=RelationshipType)\n"
                          "    time_end = time.time()\n"
                          "    time_c = time_end - time_start\n"
                          "    print('time cost', time_c, 's')\n"
                          "    return time_c\n"
                          "\n"
                          "def start_db():\n"
                          "    os.system('./neo4j start')\n"
                          "\n"
                          "def stop_db():\n"
                          "    os.system('./neo4j stop')\n"
                          "\n"
                          "def import_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/Student.csv ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/WLAN.csv ' \\\n"
                          "      '--nodes=/users/sig/song/neo4j/import/State.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Access.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Call.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/MakeUp.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/SendSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/Proximity.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/CloseFriend.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/users/sig/song/neo4j/import/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_snapshot_social_experiment():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Student.csv ' \\\n"
                          "      '--nodes=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/WLAN.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Access.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Call.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/ReceiveSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SendSMS.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/Proximity.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/BlogLivejournalTwitter.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/CloseFriend.csv ' \\\n"
                          "      '--relationships=//users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/FacebookAllTaggedPhotos.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/PoliticalDiscussant.csv ' \\\n"
                          "      '--relationships=/users/sig/song/Exp_centrality/test_modelisation/to_snapshot_SE/Snapshot_data/SocializeTwicePerWeek.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_e_commerce():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j --nodes=/users/sig/song/neo4j/import/user.csv --nodes=/users/sig/song/neo4j/import/category.csv --nodes=/users/sig/song/neo4j/import/item1_5.csv --nodes=/users/sig/song/neo4j/import/item6_10.csv --nodes=/users/sig/song/neo4j/import/item11_15.csv --nodes=/users/sig/song/neo4j/import/item16_20.csv --nodes=/users/sig/song/neo4j/import/item26_30.csv --nodes=/users/sig/song/neo4j/import/item31_34.csv --relationships=/users/sig/song/neo4j/import/addtocart.csv --relationships=/users/sig/song/neo4j/import/transaction.csv --relationships=/users/sig/song/neo4j/import/view.csv --relationships=/users/sig/song/neo4j/import/belongto.csv --relationships=/users/sig/song/neo4j/import/subCategory.csv --skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_city_bike():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "              '--nodes=/users/sig/song/neo4j/import/station.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202102_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/2020_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202101_trip.csv ' \\\n"
                          "              '--relationships=/users/sig/song/neo4j/import/202103_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202104_trip.csv ' \\\n"
                          "              #'--relationships=/users/sig/song/neo4j/import/202105_trip.csv ' \\\n"
                          "              '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_mathoverflow():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/projects/sig/song/neo4j/import/mathoverflow/user.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/answer.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/comments_to_answers.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/mathoverflow/comments_to_questions.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_wikitalk():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/projects/sig/song/neo4j/import/wiki/user_wiki.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/wiki/wiki-talk.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def import_dblp():\n"
                          "    # import data\n"
                          "    os.system('./neo4j-admin import --force --database=neo4j ' \\\n"
                          "      '--nodes=/projects/sig/song/neo4j/import/dblp/dblp_node.csv ' \\\n"
                          "      '--relationships=/projects/sig/song/neo4j/import/dblp/dblp_edge.csv ' \\\n"
                          "      '--skip-bad-relationships --skip-duplicate-nodes')\n"
                          "\n"
                          "def drop_relationships_by_percentage(driver, percentage_left):\n"
                          "    # Calculate the number of relationships to delete based on the percentage\n"
                          "    num_to_delete = int(TOTAL_NUM_RELS * (1 - percentage_left / 100))\n"
                          "    # Generate a list of ids to delete\n"
                          "    ids_to_delete = random.sample(range(TOTAL_NUM_RELS), num_to_delete)\n"
                          "    query = f\"MATCH ()-[r]-() WHERE id(r) in {str(ids_to_delete)} DELETE r\"\n"
                          "    driver.execute_query(query)\n"
                          "\n"
                          "def test(time_cost, percentage_list, EntityLabel, RelationshipType=None):\n"
                          "    j = 0\n"
                          "    for p in percentage_list:\n"
                          "        l = []\n"
                          "        import_snapshot_social_experiment()\n"
                          "        for i in range(0, 10):\n"
                          "            time_axis = pd.DatetimeIndex(time_axises[f'sub_tl{str(j*10+i)}'])\n"
                          "            if p != 100:\n"
                          "                import_snapshot_social_experiment()\n"
                          "                start_db()\n"
                          "                time.sleep(120)\n"
                          "                driver = conn()\n"
                          "                drop_relationships_by_percentage(driver, p)\n"
                          "                driver.close()\n"
                          "            else:\n"
                          "                start_db()\n"
                          "                time.sleep(60)\n"
                          "            l.append(timing(time_axis, EntityLabel, RelationshipType=RelationshipType))\n"
                          "            stop_db()\n"
                          "        print(l)\n"
                          "        time_cost[p / 100] = l\n"
                          "        j+=1\n"
                          "    return time_cost\n"
                          "\n"
                          "# Specify the Neo4j installation directory\n"
                          "os.chdir(neo4j_dir)\n"
                          "\n"
                          "time_cost = {}\n"
                          "p_l = [100]\n"
                          "\n"
                          "test(time_cost, p_l, EntityLabel='Student')\n"
                          "\n"
                          "print(time_cost)\n"
                          "\n")

        os.chdir(script_dir)
        with open(f'eigenvector_{nb_machine}_{str(i + 1)}.py', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to eigenvector_{nb_machine}_{str(i + 1)}.py")


python = '/users/sig/song/env/bin/python'


def generate_sh(nb_machine, task_name, python, partition, nb_task, cpu_per_task):
    for i in range(nb_machine):
        generated_code = f"""#!/bin/bash

#SBATCH --job-name={task_name}{nb_machine}_{str(i + 1)}
#SBATCH --output={task_name}{nb_machine}_{str(i + 1)}.out
#SBATCH --error={task_name}{nb_machine}_{str(i + 1)}.er

#SBATCH --partition={partition}

#SBATCH --ntasks={nb_task}
#SBATCH --cpus-per-task={cpu_per_task} 

python={python}

script={script_dir}/{task_name}_{nb_machine}_{str(i + 1)}.py

srun ${{python}} ${{script}}
"""
        os.chdir(script_dir)
        with open(f'{nb_machine}_{str(i + 1)}_{task_name}.sh', 'w') as file:
            file.write(generated_code)

        print(f"Generated code has been written to {task_name}_{nb_machine}_{str(i + 1)}.sh")


nb_machine = int(sys.stdin.read())

clear_folder(script_dir)

generate_py(nb_machine, '2009-01-09T00:00:00', '2009-04-24T00:00:00', 'd')
generate_sh(nb_machine, 'eigenvector', python, '24CPUNodes', '1', '24')
