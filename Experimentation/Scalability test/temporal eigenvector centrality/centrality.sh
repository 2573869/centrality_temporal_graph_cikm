#!/bin/bash

python=/users/sig/song/env/bin/python

script=create_scripts.py 

echo "11" | ${python} ${script}

shell_scripts=$(find /users/sig/song/Exp_centrality/Eigenvector/script -name "*.sh")

job_ids=""

echo "Submitting distributed sub-graph jobs..."
for script in $shell_scripts; do
    job_id=$(sbatch $script | awk '{print $4}')
    echo "Submitted job $job_id"
    job_ids+=":$job_id"
done

echo "All jobs submitted."

