***Article "Selective Evolving Centrality in Temporal Heterogeneous Graphs" for EDBT 2025***

**Proposition**

We run calculations of the eigenvector and betweenness centrality in the Example 3.5 and present the results in Table 4. 
We present the Python code of these calculations in the folder [Proposition](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Proposition?ref_type=heads). The Python libraries to install are specified in the file [requirements.txt](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Proposition/requirements.txt?ref_type=heads).


**Technical environment**

_Centralized approach_

The experiments by centralized approach are conducted on a PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. One virtual machine installed on this hardware, that has 32 GB in terms of RAM and 100 GB in terms of disk size, is used for our experiments. The Neo4j graph database is installed in the virtual machine to store and query temporal graph datasets. To avoid any bias in the disk management, the default tuning of Neo4j is kept, no customized optimization techniques are used. The programming language used to implement our metrics is Python 3.7.

Install your technical environment:
1) Install Neo4j Server 5.x.
2) Install Pycharm with the Jupyter Notebook Plugin.
3) Open Pycharm.
4) Clone this remote repository in Pycharm. It will create a pycharm project. 

The Python libraries to install are specified in the file [requirements.txt](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Centralized/requirements.txt?ref_type=heads).

_Decentralized approach_


We conducted our experiments with the Slurm cluster offered by a platform named OSIRIM (Open Services for Indexing and Research Information in Multimedia contents), which is one of the platforms of IRIT (Institut de Recherche en Informatique de Toulouse). It consists of a storage area with a capacity of approximately 1PB and a computing cluster of 928 cores and 31 GPUs in total. We installed Neo4j Server 4.x on each node. 

Our experiments were conducted on the partition named “24CPUNodes” which  consists of 12 computing nodes with dual Intel Xeon Gold 6136 processors at 3 GHz, with 24 processors and 192 GB of RAM each. We installed for each computing node one Neo4j graph database thus they could work on the same dataset independently. The programming language used is Python 3.7. 

We put in the git python files and bash files to submit to Slurm. You may need to adapt them to the system of your own.

***Datasets***

We used six datasets entitled "Social Experiment", "E-commerce", "Citibike", "Math Overflow", "wiki-talk" and "DBLP" available on [the google drive EDBT 2025](https://drive.google.com/drive/folders/1Hx7SHuCQ4ieB87xT-MOw4d-xopBiVx1j?usp=sharing). 

We transformed each dataset into the temporal property graph model of [Andriamampianina et al. 2022](https://www.sciencedirect.com/science/article/abs/pii/S0169023X22000271). Transformation details for datasets Social Experiment, "E-commerce" and "Citibike" are available in [DKE gitlab](https://gitlab.com/2573869/dke_temporal_graph_experiments). Additionally, their importation details in Neo4j are available in [DKE gitlab](https://gitlab.com/2573869/dke_temporal_graph_experiments). Code to transform the other three datasets [Math Overflow](https://snap.stanford.edu/data/sx-mathoverflow.html), [wiki-talk](https://snap.stanford.edu/data/wiki-talk-temporal.html) and [DBLP](http://konect.cc/networks/dblp_coauthor/) is available in folder [Dataset Transformation](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Dataset%20Transformation).


 For the graph model compatibility test, we used the "Social experiment" in the form of graph snapshots. The transformed dataset is also available on [the google drive EDBT 2025](https://drive.google.com/drive/folders/12dxjXHaFK3QRjrKFoR54lihGS4q-P0tn) and the transfomation details is available in python file [transfer_to_snapshot.py](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Compatibility%20test/transfer_to_snapshot.py?ref_type=heads).



**Implementation**

We put in the folder [Implementation](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation?ref_type=heads) the code for implementing our algorithm SelectiveCentralityAnalysis(SCA) in both [centralized](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/tree/main/Implementation/Centralized?ref_type=heads) and [decentralized](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/tree/main/Implementation/Decentralized?ref_type=heads) environments. There is a [schema](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/implementation.pdf) presenting both implementations. 


_Implementation in centralized environment_

In the folder [centralized](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation/Centralized), you can find directly python files of our algorithm implemented with [degree centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Centralized/DegreeCentrality.py) and [eigenvector centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Centralized/EigenvectorCentrality.py)  where they could be executed. The Python libraries to install are specified in the file [requirements.txt](https://gitlab.com/2573869/centrality_temporal_graph_edbt/Implementation/Centralized/requirements.txt).

_Implementation in decentralized environment_

In the folder [decentralized](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/tree/main/Implementation/Decentralized?ref_type=heads), we present three folders, each for the implementation a centrality metric: [betweenness centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation/Decentralized/temporal%20betweenness%20centrality),[degree centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation/Decentralized/temporal%20degree%20centrality) and [eigenvector centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation/Decentralized/temporal%20eigenvector%20centrality). They are composed are 5 files that are executed in the same way. 

We take the example of the [degree centrality](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Implementation/Decentralized/temporal%20degree%20centrality) folder:  
- a python file [create_scripts.py](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/create_scripts.py?ref_type=heads) to generate python files being executed on each computation node with time points distributed and bash files to submit them (One directory should be specified in create_scripts.py to contain files generated) 
- a bash file [create_scripts.sh](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/create_scripts.sh) to submit the python files generator  
- a python file [aggregation.py](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/aggregation.py?ref_type=heads) to aggregate results from all computation nodes
- a bash file [aggregation.sh](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/aggregation.sh) to submit the python file aggregation.py. 
- a bash file [centrality.sh](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/centrality.sh?ref_type=heads)  to call sequentially [create_scripts.sh](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/create_scripts.sh), generated bash files, and then [aggregation.sh](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/aggregation.sh).

You can find also [environment requirements](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Implementation/Decentralized/requirements.txt) for implementation and experiments in decentralized environment.

To run the implementation of a centrality metric, you are going to:
 - set parametters about metric calculation in [create_scripts.py](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/create_scripts.py?ref_type=heads)
 - set parametters about metric usage in [aggregation.py](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/aggregation.py?ref_type=heads) 
 - define the number of computation nodes to be used and specify the directory of files generated in [centrality.sh](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/centrality.sh?ref_type=heads)
 - call only the bash file [centrality.sh](https://gitlab.com/2573869/centrality_temporal_graph_kdd/-/blob/main/Implementation/Decentralized/temporal%20degree%20centrality/centrality.sh?ref_type=heads). 

**Experimentation**
 
We have done four experiments : 
1) Computation approach test (not presented in the article)
2) Compatibility test
3) Scalability test
4) Analytical performance test

The first experiment has been done in both centralized and decentralized experiments. The three other experiments have been done only on decentralized experiments.

_Computation approach test_

We tested the execution of our algorithm in two computation environments. Execution time in dicentralized environment 
has been already collected from the scalabilty test with scale factor 100%. For that in centralized environment, you can find code in folder [computation approach](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Computation%20approach%20test?ref_type=heads). We provide an analysis of the results of this experiment in the file [computation_approach_analysis](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Computation%20approach%20test/computation_approach_analysis.pdf).

_Compatibility test_

We tested the compatibility of our algorithm with two graph models, graph snapshot and the temporal property graph model of [Andriamampianina et al. 2022](https://www.sciencedirect.com/science/article/abs/pii/S0169023X22000271), on dataset "Social Experiment".

In the folder [Compatibility test](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Compatibility%20test?ref_type=heads), you can find the file [transfer_to_snapshot.py](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Compatibility%20test/transfer_to_snapshot.py?ref_type=heads) to convert the Social Experiment dataset from the temporal property graph model to the graph snapshots model.

For each metric, this experiment is done with the same [code](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Scalability%20test) of scalability test in decentralized environment, with a scale factor 100%.

_Scalability_

We tested the scalability of our algorithm by running on different datasets. Each dataset we delete randomly their relationships to keep 20%, 40%, 60%, 80% and 100% of the relationships in the dataset. This corresponds to our 5 scale factors (20%, 40%, 60%, 80% and 100%). Then, we run 10 times our algorithm on each dataset for each scale factor and records its execution times. We ran these experimentations in the decentralized environment. The code is in folder [Scalability test](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Scalability%20test). For each test in the decentralized environment, similar to implementation, you can see the 5 files for each metric. To use the code in a decentralized environment you are going to:
 - set parameters about metric calculation and chose the dataset to import in _create\_scripts.py_ 
 - define the number of computation nodes to be used and specify the directory of files generated in _centrality.sh_
 - call only the bash file _centrality.sh_

We tested how configuration impacted algorithm runtime. In folder [Semantic selection](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Scalability%20test/Semantic%20selection) there is code for runtimes of our algorithm with semantic selection (Figure 5) and in folder [Temporal Selection](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Scalability%20test/Temporal%20selection) there is code for algorithm runtimes on time window with length defined (Figure 6). The experiment about semantic selection's impact on algorithm runtimes was executed on a single machine. To use the code, you could call the python file mentioned above directly. However, the experiment about temporal selection's impact on algorithm runtimes was executed in the decentralized environment. Therefore, to use the code, you are going to:
 - set lengths of time window and chose the dataset to import in _create\_scripts.py_ 
 - define the number of computation nodes to be used and specify the directory of files generated in _centrality.sh_
 - call only the bash file _centrality.sh_
 


_Analytical Performance_

We conduct experiments to demonstrate the analytical performance of our algorithm.

In folder [Evolution](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Analytical%20performance%20test/Evolution), you can find the python file [evolution.py](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Analytical%20performance%20test/Evolution/evolution.py) to produce diagrams about centrality evolution (Figure 7 and 8) in the article.

In folder [Selectivity](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Analytical%20performance%20test/Selectivity), you can find the python file [relationship_correlation.py](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Analytical%20performance%20test/Selectivity/relationship_correlation.py) to produce the Spearman's rank correlation matrix in the Figure 9 of the article and another matrix calculated with E-commerce dataset. In the folder [Selectivity/additional](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Analytical%20performance%20test/Selectivity/additional), you can see the code to work out the Kendall Tau in Table 7.

Additionally, in folder [Selectivity](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/tree/main/Experimentation/Analytical%20performance%20test/Selectivity) there is another [diagram](https://gitlab.com/2573869/centrality_temporal_graph_edbt/-/blob/main/Experimentation/Analytical%20performance%20test/Selectivity/Similarity%20of%20entities'%20ranking%20in%20Social%20Experiment.pdf) which indicates the difference between selective and non-selective metric by presenting the similarity of entities' ranking calculated with all relationships or one type of relationships. The calculation used degree centrility metric on Social Experiment dataset. The similarity value was defined by following formula:
Similarity = 1 - | rank1 - rank2 | / (max_rank - min_rank)
 




