import pandas as pd

'''
By this file we transformed Math Overflow, wiki-talk and DBLP datasets from txt files downloaded 
from their origin websites to csv files prepared for importation of Neo4j. The transformed datasets 
are modeled with temporal property graph.
'''

'''
Math Overflow Dataset
'''
# Define input and output file paths
input_file = "sx-mathoverflow-a2q.txt"  # Replace with your actual file name
output_file = "answer.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df[':TYPE'] = 'Answer'
# Convert the timestamp column to datetime
df['startvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')# Use appropriate unit
df['endvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')
df.drop('timestamp', axis=1, inplace=True)
# Save to CSV
df.to_csv(output_file, index=False)
print(f"File converted successfully! Saved as {output_file}")

# Define input and output file paths
input_file = "sx-mathoverflow-c2q.txt"  # Replace with your actual file name
output_file = "comments_to_questions.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df[':TYPE'] = 'Comments_to_Questions'
# Convert the timestamp column to datetime
df['startvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')# Use appropriate unit
df['endvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')
df.drop('timestamp', axis=1, inplace=True)
# Save to CSV
df.to_csv(output_file, index=False)
print(f"File converted successfully! Saved as {output_file}")

# Define input and output file paths
input_file = "sx-mathoverflow-c2a.txt"  # Replace with your actual file name
output_file = "comments_to_answers.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df[':TYPE'] = 'Comments_to_Answers'
# Convert the timestamp column to datetime
df['startvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')# Use appropriate unit
df['endvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')
df.drop('timestamp', axis=1, inplace=True)
# Save to CSV
df.to_csv(output_file, index=False)
print(f"File converted successfully! Saved as {output_file}")

# Define input and output file paths
input_file = "sx-mathoverflow.txt"  # Replace with your actual file name
output_file = "user.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df_valid_timestamp = pd.concat([
    df.loc[:, [':START_ID', 'timestamp']].rename(columns={':START_ID': 'user_id:ID'}),
    df.loc[:, [':END_ID', 'timestamp']].rename(columns={':END_ID': 'user_id:ID'})
]).drop_duplicates().reset_index(drop=True)
df_start = df_valid_timestamp.groupby('user_id:ID').min()
df_end = df_valid_timestamp.groupby('user_id:ID').max()
df_user = pd.merge(df_start, df_end, on='user_id:ID')
df_user.columns=['startvalidtime','endvalidtime']
df_user['startvalidtime'] = pd.to_datetime(df_user['startvalidtime'], unit='s').dt.strftime('%Y-%m-%dT00:00:00')
df_user['endvalidtime'] = pd.to_datetime(df_user['endvalidtime'], unit='s').dt.strftime('%Y-%m-%dT23:59:59')
df_user[':LABEL'] = 'User'
df_user.to_csv(output_file, index=True)
print(f"File converted successfully! Saved as {output_file}")

'''
wiki'talk Dataset
'''
# Define input and output file paths
input_file = "wiki-talk-temporal.txt"  # Replace with your actual file name
output_file = "wiki-talk.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df[':TYPE'] = 'Editing'
# Convert the timestamp column to datetime
df['startvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')# Use appropriate unit
df['endvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')
df.drop('timestamp', axis=1, inplace=True)
# Save to CSV
df.to_csv(output_file, index=False)
print(f"File converted successfully! Saved as {output_file}")

# Define input and output file paths
input_file = "wiki-talk-temporal.txt"  # Replace with your actual file name
output_file = "user_wiki.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=' ', header=None, names=[':START_ID', ':END_ID', 'timestamp'])
df_valid_timestamp = pd.concat([
    df.loc[:, [':START_ID', 'timestamp']].rename(columns={':START_ID': 'user_id:ID'}),
    df.loc[:, [':END_ID', 'timestamp']].rename(columns={':END_ID': 'user_id:ID'})
]).drop_duplicates().reset_index(drop=True)
df_start = df_valid_timestamp.groupby('user_id:ID').min()
df_end = df_valid_timestamp.groupby('user_id:ID').max()
df_user = pd.merge(df_start, df_end, on='user_id:ID')
df_user.columns=['startvalidtime','endvalidtime']
df_user['startvalidtime'] = pd.to_datetime(df_user['startvalidtime'], unit='s').dt.strftime('%Y-%m-%dT00:00:00')
df_user['endvalidtime'] = pd.to_datetime(df_user['endvalidtime'], unit='s').dt.strftime('%Y-%m-%dT23:59:59')
df_user[':LABEL'] = 'User'
df_user.to_csv(output_file, index=True)
print(f"File converted successfully! Saved as {output_file}")

'''
DBLP Dataset
'''
# Define input and output file paths
input_file = "dblp.txt"  # Replace with your actual file name
output_file = "dblp_edge.csv"
# Read the space-separated text file
df = pd.read_csv(input_file, sep=r'\s+', header=None, names=[':START_ID', ':END_ID', 'weight', 'timestamp'])
df[':TYPE'] = 'Cooperation'
# Convert the timestamp column to datetime
df['startvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')# Use appropriate unit
df['endvalidtime'] = pd.to_datetime(df['timestamp'], unit='s').dt.strftime('%Y-%m-%dT%H:%M:%S')
df.drop('timestamp', axis=1, inplace=True)
# Save to CSV
df.to_csv(output_file, index=False)
print(f"File converted successfully! Saved as {output_file}")

# Define input and output file paths
input_file = "dblp.txt"  # Replace with your actual file name
output_file = "dblp_node.csv"
# Read the space-separated text file
pd.read_csv(input_file, sep=r'\s+', header=None, names=[':START_ID', ':END_ID', 'weight', 'timestamp'])
df_valid_timestamp = pd.concat([
    df.loc[:, [':START_ID', 'timestamp']].rename(columns={':START_ID': 'user_id:ID'}),
    df.loc[:, [':END_ID', 'timestamp']].rename(columns={':END_ID': 'user_id:ID'})
]).drop_duplicates().reset_index(drop=True)
df_start = df_valid_timestamp.groupby('user_id:ID').min()
df_end = df_valid_timestamp.groupby('user_id:ID').max()
df_user = pd.merge(df_start, df_end, on='user_id:ID')
df_user.columns=['startvalidtime','endvalidtime']
df_user['startvalidtime'] = pd.to_datetime(df_user['startvalidtime'], unit='s').dt.strftime('%Y-%m-%dT00:00:00')
df_user['endvalidtime'] = pd.to_datetime(df_user['endvalidtime'], unit='s').dt.strftime('%Y-%m-%dT23:59:59')
df_user[':LABEL'] = 'Author'
df_user.to_csv(output_file, index=True)
print(f"File converted successfully! Saved as {output_file}")

